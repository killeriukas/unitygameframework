﻿using TMI.Core;

namespace TMI.LocalNotificationManagement {

    public class EnableLocalNotificationCommand : BaseGameCommand {

        private bool enable;

        public EnableLocalNotificationCommand(IInitializer initializer, PlayerProfile playerProfile) : base(initializer, playerProfile) {
        }

        public void Setup(bool enable) {
            this.enable = enable;
        }

        public override void Execute() {
            profile.EnableLocalNotification(enable);
            profile.Save(serializer);
        }

    }


}