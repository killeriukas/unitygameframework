﻿using System;

namespace TMI.LocalNotificationManagement {

    public interface ILocalNotificationRequest {
        TimeSpan duration { get; }
        string title { get; }
        string body { get; }
        string smallIconName { get; }
    }

}