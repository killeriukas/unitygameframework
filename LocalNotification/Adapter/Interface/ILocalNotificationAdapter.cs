﻿using System;

namespace TMI.LocalNotificationManagement {

    public interface ILocalNotificationAdapter : IDisposable {
        void Initialize();
        event Action<ILocalNotificationRequest> onNotificationClicked;
        void ClearAllLocalNotifications();
        void ScheduleLocalNotification(ILocalNotificationRequest request);
    }

}