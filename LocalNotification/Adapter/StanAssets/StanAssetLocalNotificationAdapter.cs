﻿using SA.CrossPlatform.Notifications;
using SA.Foundation.Templates;
using System;
using TMI.LogManagement;
using UnityEngine;

namespace TMI.LocalNotificationManagement {

    public class StanAssetLocalNotificationAdapter : ILocalNotificationAdapter, ILoggable {

        private int uniqueRequestId = 0;
        private UM_iNotificationsClient notificationClient;

        public event Action<ILocalNotificationRequest> onNotificationClicked;

        public void Initialize() {
            Assert.IsNotNull(notificationClient, "Cannot initialize StanAssetLocalNotificationAdapter twice!");

            notificationClient = UM_NotificationCenter.Client;

            UM_NotificationRequest lastOpenedNotification = notificationClient.LastOpenedNotificationt; //store it here, because it's a call to Android
            if(lastOpenedNotification != null) {
                OnNotificationClicked(lastOpenedNotification);
            }

            notificationClient.OnNotificationClick.AddListener(OnNotificationClicked);
        }

        private void OnNotificationClicked(UM_NotificationRequest request) {
            UM_Notification content = request.Content;

            LocalNotificationRequest localNotification = new LocalNotificationRequest(
                content.Title,
                content.Body,
                TimeSpan.Zero);

            onNotificationClicked?.Invoke(localNotification);
        }

        public void ScheduleLocalNotification(ILocalNotificationRequest request) {
            UM_Notification content = new UM_Notification();

            content.SetTitle(request.title);
            content.SetBody(request.body);

            if(!string.IsNullOrEmpty(request.smallIconName)) {
                content.SetSmallIconName(request.smallIconName);
            }
            
            //content.SetSoundName("mySound.wav");

            UM_TimeIntervalNotificationTrigger trigger = new UM_TimeIntervalNotificationTrigger((int)request.duration.TotalSeconds);
            UM_NotificationRequest um_request = new UM_NotificationRequest(uniqueRequestId++, content, trigger);

            Action<SA_Result> onComplete = (result) => {
                if(result.IsSucceeded) {
                    Logging.Log(this, "Successfully scheduled a local notification request.");
                } else {
                    Logging.LogError(this, "Local notification failed to be scheduled due to [{0}].", result.Error.FullMessage);
                }
            };

            notificationClient.AddNotificationRequest(um_request, onComplete);
        }

        public void ClearAllLocalNotifications() {
            Logging.Log(this, "Removing all local notifications.");
            notificationClient.RemoveAllDeliveredNotifications();
            notificationClient.RemoveAllPendingNotifications();

            //TODO: hack for Stans local notifications. Once they removed their ids they are deleted from PlayerPrefs, but they do not SAVE it.
            PlayerPrefs.Save();
        }

        public void Dispose() {
            notificationClient.OnNotificationClick.RemoveListener(OnNotificationClicked);
        }
    }

}