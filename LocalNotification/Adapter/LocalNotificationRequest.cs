﻿using System;

namespace TMI.LocalNotificationManagement {

    public class LocalNotificationRequest : ILocalNotificationRequest {

        public string title { get; private set; }
        public string body { get; private set; }
        public TimeSpan duration { get; private set; }

        public string smallIconName { get; set; }

        public LocalNotificationRequest(string title, string body, TimeSpan duration) {
            Assert.IsTrue<ArgumentNullException>(string.IsNullOrEmpty(title), "Local notification title cannot be empty!");
            Assert.IsTrue<ArgumentNullException>(string.IsNullOrEmpty(body), "Local notification body cannot be empty!");

            this.title = title;
            this.body = body;
            this.duration = duration;
        }

        public LocalNotificationRequest(string title, string body, TimeSpan duration, string smallIconName) : this(title, body, duration) {
            Assert.IsTrue<ArgumentNullException>(string.IsNullOrEmpty(smallIconName), "Local notification small icon name cannot be empty!");

            this.smallIconName = smallIconName;
        }

    }

}