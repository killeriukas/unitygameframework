﻿using System;
using TMI.AnalyticsManagement;
using TMI.Core;
using TMI.Helper;
using TMI.LogManagement;

namespace TMI.LocalNotificationManagement {

    public abstract class BaseLocalNotificationManager : BaseNotificationManager, ILocalNotificationManager {

        private enum State {
            Stopped = 0,
            Running
        }

        private State state = State.Stopped;

        private IAnalyticsManager analyticsManager;
        private ILocalNotificationAdapter adapter;
        private ILocalNotification localNotification;

        public override void Setup(IInitializer initializer, bool isNew) {
            base.Setup(initializer, isNew);
            analyticsManager = initializer.GetManager<IAnalyticsManager>();
        }

        public void Initialize(ILocalNotificationAdapter adapter, ILocalNotification localNotification) {
            Assert.IsTrue<InvalidOperationException>(state == State.Running, "Cannot initialize local notification manager twice!");

            this.localNotification = localNotification;
            this.adapter = adapter;

            adapter.onNotificationClicked += OnLocalNotificationClicked;
            adapter.Initialize();
            adapter.ClearAllLocalNotifications();

            Listen<GamePausedNotification>(this, OnApplicationPaused);
            Listen<GameUnpausedNotification>(this, OnApplicationUnpaused);

            state = State.Running;
        }

        protected virtual void OnLocalNotificationClicked(ILocalNotificationRequest request) {
            Logging.Log(this, "User has clicked on the local notification [{0}] outside of the game!", request.title);

            GenericAnalyticsTrackerEvent genericAnalyticsTrackerEvent = AnalyticsHelper.CreateButtonClickEvent("os", request.title);
            analyticsManager.LogEvent(genericAnalyticsTrackerEvent);
        }

        private void OnApplicationUnpaused(GameUnpausedNotification notification) {
            adapter.ClearAllLocalNotifications();
        }

        private void OnApplicationPaused(GamePausedNotification notification) {
            if(localNotification.isEnabled) {
                ScheduleNotifications(adapter);
            }
        }

        protected abstract void ScheduleNotifications(ILocalNotificationAdapter adapter);

        protected override void OnDestroy() {
            adapter.onNotificationClicked -= OnLocalNotificationClicked;
            state = State.Stopped;
            base.OnDestroy();
        }

    }

}