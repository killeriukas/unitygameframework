﻿using TMI.Notification;

namespace TMI.LocalNotificationManagement {

    public class LocalNotificationStatusChangedNotification : INotification {

        public bool isEnabled { get; private set; }

        public LocalNotificationStatusChangedNotification(bool isEnabled) {
            this.isEnabled = isEnabled;
        }

    }

}