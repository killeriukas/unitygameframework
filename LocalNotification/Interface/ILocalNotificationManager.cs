﻿using TMI.Core;

namespace TMI.LocalNotificationManagement {

    public interface ILocalNotificationManager : IManager {
        void Initialize(ILocalNotificationAdapter adapter, ILocalNotification localNotification);
    }

}