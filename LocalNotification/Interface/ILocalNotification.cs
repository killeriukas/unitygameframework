﻿
namespace TMI.LocalNotificationManagement {

    public interface ILocalNotification {
        bool isEnabled { get; }
    }

}