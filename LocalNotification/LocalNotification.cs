﻿using Newtonsoft.Json;
using System;
using TMI.Save;

namespace TMI.LocalNotificationManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class LocalNotification : BaseModelGeneric<LocalNotification.Data>, ILocalNotification {

        [Serializable]
        public class Data : IModel {
            public bool enabled = false;
        }

        public bool isEnabled { get { return model.enabled; } }

        public void SetEnable(bool enable) {
            model.enabled = enable;
        }

    }
}