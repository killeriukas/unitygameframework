﻿using System;
using TMI.Helper;
using TMI.LogManagement;
using UnityEngine;

#if USE_FACEBOOK_AUDIENCE_NETWORK
using AudienceNetwork;
#endif

namespace TMI.Ads {

    public class FacebookAudienceAdsProviderAdapter : IAdsProviderAdapter, ILoggable {

        //TODO: FIX THIS ONE, cuz it seems that it won't work!!!!!!!!!!!!!!!!!!!!!

        private GameObject rewardedAdGameObject;

#if USE_FACEBOOK_AUDIENCE_NETWORK
        private RewardedVideoAd rewardedVideoAd;
#endif

        private Action<AdsManager.AdResult> onAdFinished;

        private readonly string rewardedAdPlacementId;

        public bool isRewardedAdReady { get; private set; }

        private bool wasRewardAdConsumed;

        public FacebookAudienceAdsProviderAdapter(Transform parentTransform, string rewardedAdPlacementId) {
            this.rewardedAdPlacementId = rewardedAdPlacementId;
            this.isRewardedAdReady = false;
            this.onAdFinished = null;

            rewardedAdGameObject = new GameObject("FBA_RewardedAd");
            rewardedAdGameObject.transform.SetParent(parentTransform, false);

#if USE_FACEBOOK_AUDIENCE_NETWORK
            rewardedAdGameObject.AddComponent<AdHandler>(); //this one is not added in the test scene, but is used by RewardedVideoAd behind the scenes
#endif

            wasRewardAdConsumed = true;
        }

        public void OnRegister() {
//#if !UNITY_EDITOR
//            Assert.IsNotNull(rewardedVideoAd, "Cannot initialize FBA ads again, once they are initialized!");

//            rewardedVideoAd = new RewardedVideoAd(rewardedAdPlacementId);
//            rewardedVideoAd.Register(rewardedAdGameObject);


//            // Set delegates to get notified on changes or when the user interacts with the ad.
//            rewardedVideoAd.RewardedVideoAdDidLoad = OnRewardedAdLoaded;
//            rewardedVideoAd.RewardedVideoAdDidFailWithError = OnRewardedAdFailedToLoad;
//            rewardedVideoAd.RewardedVideoAdWillLogImpression = OnRewardedAdFinished;

//            rewardedVideoAd.RewardedVideoAdDidClose = OnRewardedAdClosed;
//            //this.rewardedVideoAd.RewardedVideoAdDidClose = (delegate () {
//            //    Debug.Log("Rewarded video ad did close.");
//            //    if(this.rewardedVideoAd != null) {
//            //        this.rewardedVideoAd.Dispose();
//            //    }
//            //});


//            // Initiate the request to load the ad.
//            rewardedVideoAd.LoadAd();
//#endif
        }

        private void OnRewardedAdFinished() {
            Logging.Log(this, "Rewarded ad watched successfully.");
            wasRewardAdConsumed = true;
            GeneralHelper.CallNullCheck(onAdFinished, AdsManager.AdResult.Finished);
            onAdFinished = null;
        }

        private void OnRewardedAdClosed() {
            //TODO: this may be called all the time when the ad has finished and been dismissed.
            //was not tested yet, so might need to remove the callback from here!
            Logging.Log(this, "Rewarded ad closed.");

            if(!wasRewardAdConsumed) {
                Logging.Log(this, "Rewarded ad was not consumed, hence calling skipped callback.");
                wasRewardAdConsumed = true;
                GeneralHelper.CallNullCheck(onAdFinished, AdsManager.AdResult.Skipped);
                onAdFinished = null;
            }

            //TODO: load a new ad here later on
        }

        private void OnRewardedAdFailedToLoad(string error) {
            Logging.LogWarning(this, "Rewarded ad failed to load with error [{0}]!", error);
        }

        private void OnRewardedAdLoaded() {
            Logging.Log(this, "Rewarded video loaded.");
            isRewardedAdReady = true;
            wasRewardAdConsumed = false;
        }

        public void ShowRewardedAd(Action<AdsManager.AdResult> onSuccessCallback) {
            Assert.IsFalse<InvalidOperationException>(isRewardedAdReady, "Cannot show FBA ad when it's not ready!");

            onAdFinished = onSuccessCallback;

#if USE_FACEBOOK_AUDIENCE_NETWORK
            rewardedVideoAd.Show();
#endif
            isRewardedAdReady = false;
        }

        public void Dispose() {
            isRewardedAdReady = false;
#if USE_FACEBOOK_AUDIENCE_NETWORK
            GeneralHelper.DisposeAndMakeDefault(ref rewardedVideoAd);
#endif
            GameObject.Destroy(rewardedAdGameObject);
            onAdFinished = null;
        }

    }


}