﻿using System;
using TMI.LogManagement;

#if USE_UNITY_ADS
using UnityEngine.Advertisements;
#endif

namespace TMI.Ads {

    public class UnityAdsProviderAdapter : IAdsProviderAdapter, ILoggable {

        private const string rewardedVideoAd = "rewardedVideo";

        private readonly string gameAdsId;
        private readonly bool isTestMode;

        public UnityAdsProviderAdapter(string androidId, string iosId, bool isTestMode) {
            this.isTestMode = isTestMode;

#if UNITY_ANDROID
            this.gameAdsId = androidId;
#elif UNITY_IOS
            this.gameAdsId = iosId;
#else
            //do nothing, cuz there are no ads for any other platform but Android and iOS
#endif

        }

        public bool isRewardedAdReady {
            get {
#if USE_UNITY_ADS
                bool isReady = Advertisement.IsReady(rewardedVideoAd);
                return isReady;
#else
                return true;
#endif
            }
        }

        public void Dispose() {
            
        }

        public void OnRegister() {
#if USE_UNITY_ADS
            Assert.IsTrue<InvalidOperationException>(Advertisement.isInitialized, "Cannot initialize Unity ads again, once they are initialized!");

            bool isSupported = Advertisement.isSupported;
            
            if(isSupported) {
                Logging.Log(this, "Initializing Unity ads provider.");
                Advertisement.Initialize(gameAdsId, isTestMode);
            } else {
                Logging.Log(this, "Unity ads provider is not supported on this platform.");
            }
#endif
        }

        public void ShowRewardedAd(Action<AdsManager.AdResult> onSuccessCallback) {
            Assert.IsFalse<InvalidOperationException>(isRewardedAdReady, "Cannot show Unity ad when it's not ready!");

#if USE_UNITY_ADS
            Action<ShowResult> adsResultAdapter = (result) => {
                switch(result) {
                    case ShowResult.Failed:
                        onSuccessCallback(AdsManager.AdResult.Failed);
                        break;
                    case ShowResult.Finished:
                        onSuccessCallback(AdsManager.AdResult.Finished);
                        break;
                    case ShowResult.Skipped:
                        onSuccessCallback(AdsManager.AdResult.Skipped);
                        break;
                }
            };

            ShowOptions showOptions = new ShowOptions();
            showOptions.resultCallback = adsResultAdapter;
            Advertisement.Show(rewardedVideoAd, showOptions);
#else
            onSuccessCallback(AdsManager.AdResult.Finished);
#endif
        }
    }


}