﻿
using System;

namespace TMI.Ads {

    public interface IAdsProviderAdapter : IDisposable {
        bool isRewardedAdReady { get; }

        void OnRegister();
        void ShowRewardedAd(Action<AdsManager.AdResult> onSuccessCallback);
    }


}