﻿using GoogleMobileAds.Api;
using System;
using TMI.Core;
using TMI.Helper;
using TMI.LogManagement;
using TMI.Operation;
using TMI.TimerManagement;
using UnityEngine;

namespace TMI.Ads {

    public class AdMobProviderAdapter : IAdsProviderAdapter, ILoggable, IUpdatable {

        private enum ResponseStatus {
            Unknown,
            Sent,
            Success,
            Skipped,
        }

        //TODO: the adapter tries to fetch an ad for 3 times. if it fails, for example, due to no internet connection, it will stop retrying.
        //NEED TO ADD TIMER BASED RETRY LATER ON - for now, let's say if there is no ad after 3 times, don't bother.

        private readonly string appId;
        private RewardBasedVideoAd rewardBasedVideo;
        private string rewardAdUnitId; //maybe have a list later on

        private const int maxAdReloadRetryCount = 3;
        private int currentAdReloadRetryCount = 0;
        private static readonly TimeSpan reloadAdFrequencyDuration = TimeSpan.FromMinutes(1);
        private static readonly TimeSpan reloadAdFrequencyAfterMaxTriesDuration = TimeSpan.FromMinutes(5);

        //DEBUG STARTS
        private readonly bool isTestMode;
        private readonly string testDeviceId;
        private readonly bool useTestDevice;
        //DEBUG ENDS

        private ResponseStatus responseStatus = ResponseStatus.Unknown;
        private readonly IExecutionManager executionManager;
        private readonly ITimerManager timerManager;

        private IHandle loadAdTimerHandle = null;

        private Action<AdsManager.AdResult> onAdFinished;

        public AdMobProviderAdapter(IInitializer initializer, string androidId, string iosId, bool isTestMode, bool useTestDevice, string testDeviceId) {
            this.executionManager = initializer.GetManager<ExecutionManager, IExecutionManager>();
            this.timerManager = initializer.GetManager<TimerManager, ITimerManager>();
            this.useTestDevice = useTestDevice;
            this.testDeviceId = testDeviceId;
            this.isTestMode = isTestMode;

#if UNITY_ANDROID
            appId = androidId;
#elif UNITY_IPHONE
            appId = iosId;
#else
            appId = "unexpected_platform";
#endif
        }

        public void AddRewardedAdUnitId(string androidAdUnitId, string iosAdUnitId) {
            if(isTestMode) {
#if UNITY_ANDROID
                rewardAdUnitId = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IPHONE
                rewardAdUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
                rewardAdUnitId = "unexpected_platform";
#endif
            } else {
                //might need to have a list of different ad unit ids and then request to load all of them
                //for now it's just one at a time

#if UNITY_ANDROID
                rewardAdUnitId = androidAdUnitId;
#elif UNITY_IPHONE
                rewardAdUnitId = iosAdUnitId;
#else
                rewardAdUnitId = "unexpected_platform";
#endif
            }

        }

        public bool isRewardedAdReady {
            get {
#if UNITY_EDITOR
                return true;
#else
                return rewardBasedVideo.IsLoaded();
#endif
            }
        }

        public void Dispose() {
            if(loadAdTimerHandle != null) {
                loadAdTimerHandle.Cancel();
                loadAdTimerHandle = null;
            }

            if(responseStatus != ResponseStatus.Unknown) {
                executionManager.Unregister(this);
                responseStatus = ResponseStatus.Unknown;
                onAdFinished = null;
            }
        }

        public void OnRegister() {
            Assert.IsTrue<InvalidOperationException>(string.IsNullOrEmpty(rewardAdUnitId), "Set rewardAdUnitId before initializing AdMob!");

            Logging.Log(this, "Initializing AdMob.");

            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(appId);

            // Get singleton reward based video ad reference.
            rewardBasedVideo = RewardBasedVideoAd.Instance;

            //TODO: STARTS
            //callbacks can be called in any order, due to them being called on another thread.
            //this means that we have to wait for a frame to actually allow for all the callbacks to come back
            //and only then do our stuff - like call our own callback
            //TODO: ENDS

            //// Called when an ad request has successfully loaded.
            rewardBasedVideo.OnAdLoaded += OnRewardVideoAdLoaded;

            // Called when an ad request failed to load.
            rewardBasedVideo.OnAdFailedToLoad += OnRewardVideoAdFailedToLoad;

            // Called when the user should be rewarded for watching a video.
            rewardBasedVideo.OnAdRewarded += OnRewardVideoAdFinished;

            // Called when the ad is closed.
            rewardBasedVideo.OnAdClosed += OnRewardVideoAdClosed;

            RequestNewAd();
        }

        private AdRequest CreateAdRequest() {
            // Create an empty ad request.
            AdRequest.Builder builder = new AdRequest.Builder();

            if(useTestDevice) {
                builder = builder.AddTestDevice(testDeviceId);
            }

            AdRequest request = builder.Build();
            return request;
        }

        private void OnRewardVideoAdLoaded(object sender, EventArgs args) {
            Logging.Log(this, "Reward ad loaded from the [{0}] network.", rewardBasedVideo.MediationAdapterClassName());
            currentAdReloadRetryCount = 0;
        }

        private void OnRewardVideoAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
            Logging.LogWarning(this, "Reward ad failed to load with error [{0}]!", args.Message);

            if(currentAdReloadRetryCount < maxAdReloadRetryCount) {
                ++currentAdReloadRetryCount;

                Logging.Log(this, "Setting a timer to reload the ad once more [{0}]/[{1}] in [{2}].", currentAdReloadRetryCount, maxAdReloadRetryCount, reloadAdFrequencyDuration);

                loadAdTimerHandle = timerManager.Register(reloadAdFrequencyDuration, OnReloadTimerExpired);
            } else {
                Logging.Log(this, "Setting a timer to reload the ad, but this time in [{0}].", reloadAdFrequencyAfterMaxTriesDuration);
                loadAdTimerHandle = timerManager.Register(reloadAdFrequencyAfterMaxTriesDuration, OnReloadTimerExpired);
            }
            
        }

        private void OnReloadTimerExpired() {
            Logging.Log(this, "Reload timer expired. Requesting new ad!");
            loadAdTimerHandle = null;
            RequestNewAd();
        }

        private void OnRewardVideoAdFinished(object sender, Reward reward) {
            Logging.Log(this, "Reward ad watched successfully. Here comes the reward!");
            responseStatus = ResponseStatus.Success;
        }

        private void OnRewardVideoAdClosed(object sender, EventArgs args) {
            Logging.Log(this, "Frame count: [" + Time.frameCount + "] Reward ad closed.");

            //don't overwrite the success response
            if(responseStatus != ResponseStatus.Success) {
                responseStatus = ResponseStatus.Skipped;
            }

            RequestNewAd();
        }

        private void RequestNewAd() {
            Logging.Log(this, "Requesting to load a new ad.");

            // Create an empty ad request.
            AdRequest request = CreateAdRequest();

            // Load the rewarded video ad with the request.
            rewardBasedVideo.LoadAd(request, rewardAdUnitId);
        }

        public void ShowRewardedAd(Action<AdsManager.AdResult> onSuccessCallback) {
            Assert.IsFalse<InvalidOperationException>(rewardBasedVideo.IsLoaded(), "Reward video is not loaded! Check isRewardedAdReady before calling Show()!");
            Assert.IsFalse<InvalidOperationException>(responseStatus == ResponseStatus.Unknown, "Cannot show ad whilst adapter is out of sync! Current state: " + responseStatus.ToString());

            Logging.Log(this, "Show ad from the [{0}] network.", rewardBasedVideo.MediationAdapterClassName());

            onAdFinished = onSuccessCallback;
            responseStatus = ResponseStatus.Sent;
            executionManager.Register(this, OnUpdate);

#if UNITY_EDITOR
            OnRewardVideoAdFinished(null, null);
#else
            rewardBasedVideo.Show();
#endif
        }

        private ExecutionManager.Result OnUpdate() {

            if(responseStatus != ResponseStatus.Sent) {
                Logging.Log(this, "AdMob show ad response status [{0}].", responseStatus.ToString());
                switch(responseStatus) {
                    case ResponseStatus.Success:
                        GeneralHelper.CallNullCheck(onAdFinished, AdsManager.AdResult.Finished);
                        onAdFinished = null;
                        break;
                    case ResponseStatus.Skipped:
                        GeneralHelper.CallNullCheck(onAdFinished, AdsManager.AdResult.Skipped);
                        onAdFinished = null;
                        break;
                }

                responseStatus = ResponseStatus.Unknown;
                return ExecutionManager.Result.Finish;
            }

            return ExecutionManager.Result.Continue;
        }
    }
}