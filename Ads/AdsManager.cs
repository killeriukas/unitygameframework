﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.TimeManagement;

namespace TMI.Ads {

    public class AdsManager : BaseNotificationManager, IAdsManager {

        public static bool enableSmartAds = false;
        private readonly static TimeSpan cooldownDuration = TimeSpan.FromSeconds(120);
        private MonotonicTime cooldownExpirationTime;

        public enum AdResult {
            Failed = 0,
            Skipped,
            Finished
        }

        private readonly List<IAdsProviderAdapter> adProviderByPriority = new List<IAdsProviderAdapter>();

        public bool isAnySmartRewardedAdAvailable {
            get {
                if(enableSmartAds) {
                    return hasSmartAd && isAnyRewardedAdAvailable;
                } else {
                    return isAnyRewardedAdAvailable;
                }
            }
        }

        private bool hasSmartAd {
            get {
                if(enableSmartAds) {
                    return cooldownExpirationTime < MonotonicTime.now;
                } else {
                    return false;
                }
            }
        }

        public void Register(IAdsProviderAdapter adProvider) {
            adProvider.OnRegister();
            adProviderByPriority.Add(adProvider);
        }

        public bool ShowRewardedAd(Action<AdResult> onSuccessCallback) {
            IAdsProviderAdapter foundAdsProvider;
            bool isAdReady = TryFindingAvailableRewardedAdProvider(out foundAdsProvider);

            if(isAdReady) {
                foundAdsProvider.ShowRewardedAd(onSuccessCallback);
                cooldownExpirationTime = MonotonicTime.now + cooldownDuration;
            }

            return isAdReady;
        }

        private bool TryFindingAvailableRewardedAdProvider(out IAdsProviderAdapter foundAdsProvider) {
            foreach(IAdsProviderAdapter adProvider in adProviderByPriority) {
                if(adProvider.isRewardedAdReady) {
                    foundAdsProvider = adProvider;
                    return true;
                }
            }
            foundAdsProvider = null;
            return false;
        }

        public bool isAnyRewardedAdAvailable {
            get {
                foreach(IAdsProviderAdapter adProvider in adProviderByPriority) {
                    if(adProvider.isRewardedAdReady) {
                        return true;
                    }
                }
                return false;
            }
        }

        protected override void OnDestroy() {
            foreach(IAdsProviderAdapter adProvider in adProviderByPriority) {
                adProvider.Dispose();
            }
            adProviderByPriority.Clear();
            base.OnDestroy();
        }
    }

}