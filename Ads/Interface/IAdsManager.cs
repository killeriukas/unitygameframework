﻿using System;
using TMI.Core;
using UnityEngine;

namespace TMI.Ads {

    public interface IAdsManager : IManager {
        bool isAnySmartRewardedAdAvailable { get; }
        void Register(IAdsProviderAdapter adProvider);
        bool ShowRewardedAd(Action<AdsManager.AdResult> onSuccessCallback);
        bool isAnyRewardedAdAvailable { get; }
        Transform transform { get; }
    }

}