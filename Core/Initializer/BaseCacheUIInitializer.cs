﻿using TMI.AssetManagement;
using TMI.Operation;

namespace TMI.Core {

	public abstract class BaseCacheUIInitializer : BaseMainInitializer {

        protected override void Awake() {
			base.Awake();

			ISceneGroup sceneGroup = CreateUIScenesCache();
			Assert.IsNull(sceneGroup, "Scene cache cannot be null! If you don't need it, inherit from BaseInitializer instead!");

			IAsyncOperation<ISceneGroupComplete> defaultAsyncOperation = DefaultAsyncOperation<ISceneGroupComplete>.Create(OnUISceneGroupLoaded);
			sceneManager.LoadSceneGroup(sceneGroup, defaultAsyncOperation);
		}

		private void OnUISceneGroupLoaded(ISceneGroupComplete sceneGroupComplete) {
			uiManager.PrecacheUI(sceneGroupComplete);
			OnUIScenesCached();
		}

		protected abstract void OnUIScenesCached();
		protected abstract ISceneGroup CreateUIScenesCache();

	}


}