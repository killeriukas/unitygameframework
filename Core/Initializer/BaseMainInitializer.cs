﻿using TMI.Ads;
using TMI.AnalyticsManagement;
using TMI.Data;
using TMI.InAppPurchase;
using TMI.LocalNotificationManagement;
using TMI.Notification;
using TMI.SceneManagement;
using TMI.TimerManagement;
using TMI.UI;

namespace TMI.Core {

    public abstract class BaseMainInitializer : BaseInitializer {

//#pragma warning disable warning-list  
        private IExecutionManager executionManager;
//#pragma warning restore warning-list  

        protected INotificationManager notificationManager { get; private set; }
        protected SceneManager sceneManager { get; private set; }
        protected IUIManager uiManager { get; private set; }
        protected IAnalyticsManager analyticsManager { get; private set; }
        protected IPauseManager pauseManager { get; private set; }
        protected IAdsManager adsManager { get; private set; }
        protected IIapManager iapManager { get; private set; }
        protected IDataManager dataManager { get; private set; }
        protected TimerManager timerManager { get; private set; }

        protected override void RegisterManagers(IAcquirer acquirer) {
            executionManager = acquirer.AcquireManager<ExecutionManager, IExecutionManager>();

            notificationManager = acquirer.AcquireManager<NotificationManager, INotificationManager>();
            sceneManager = acquirer.AcquireManager<SceneManager>();
            uiManager = acquirer.AcquireManager<UIManager, IUIManager>();
            analyticsManager = AcquireAnalyticsManager(acquirer);
            pauseManager = acquirer.AcquireManager<PauseManager, IPauseManager>();
            adsManager = acquirer.AcquireManager<AdsManager, IAdsManager>();
            iapManager = acquirer.AcquireManager<IapManager, IIapManager>();
            dataManager = acquirer.AcquireManager<DataManager, IDataManager>();
            timerManager = acquirer.AcquireManager<TimerManager>();
        }

        protected abstract IAnalyticsManager AcquireAnalyticsManager(IAcquirer acquirer);
    }

}