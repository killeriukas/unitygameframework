﻿using TMI.Core;
using TMI.Helper;

namespace TMI.InAppPurchase {

    public abstract class BaseIapReward {

        protected readonly IapStoreData data;
        protected readonly IInitializer initializer;

        public abstract string description { get; }

        public abstract void Apply(PlayerProfile playerProfile);

        public abstract bool CanAfford();

        protected BaseIapReward(IInitializer initializer, IapStoreData data) {
            this.initializer = initializer;
            this.data = data;
        }

        public static BaseIapReward Create(IInitializer initializer, IapStoreData data) {
            BaseIapReward baseIapReward = GeneralHelper.CreateInstance<BaseIapReward>("TMI.InAppPurchase", data.rewardType, initializer, data);
            return baseIapReward;
        }

    }

}