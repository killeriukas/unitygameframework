﻿using TMI.Core;
using TMI.Number;

namespace TMI.InAppPurchase {

    public class CurrencyIapReward : BaseIapReward {

        private readonly Currency rewardCurrency;

        public CurrencyIapReward(IInitializer initializer, IapStoreData data) : base(initializer, data) {
            rewardCurrency = new Currency(data.unlockItemId, data.amount);
        }

        public override string description {
            get {
                throw new System.NotImplementedException("Use IIapProduct.description instead.");
            }
        }

        public override void Apply(PlayerProfile playerProfile) {
            playerProfile.AddCurrency(rewardCurrency);
        }

        public override bool CanAfford() {
            return true;
        }
    }


}