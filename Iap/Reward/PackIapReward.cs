﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.Data;

namespace TMI.InAppPurchase {

    public class PackIapReward : BaseIapReward {

        private List<BasePackReward> rewardList = new List<BasePackReward>();
        
        public PackIapReward(IInitializer initializer, IapStoreData data) : base(initializer, data) {
            IDataManager dataManager = initializer.GetManager<IDataManager>();

            IapPackData iapPackData = dataManager.GetDataById<IapPackData>(data.unlockItemId);

            CreateAndAddPackReward(iapPackData.bonusType0, iapPackData.bonusId0, iapPackData.bonusAmount0);
            CreateAndAddPackReward(iapPackData.bonusType1, iapPackData.bonusId1, iapPackData.bonusAmount1);
            CreateAndAddPackReward(iapPackData.bonusType2, iapPackData.bonusId2, iapPackData.bonusAmount2);
        }

        private void CreateAndAddPackReward(string className, string id, int amount) {
            BasePackReward baseReward = BasePackReward.Create(initializer, className, id, amount);
            rewardList.Add(baseReward);
        }

        public override string description => throw new NotImplementedException("Use IInAppPurchaseStorage.description instead.");

        public override void Apply(PlayerProfile playerProfile) {
            foreach(BasePackReward pack in rewardList) {
                pack.Apply(playerProfile);
            }

            playerProfile.ConsumePack(data.unlockItemId);
        }

        public override bool CanAfford() {
            return true;
        }

    }

}