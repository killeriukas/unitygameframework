﻿using TMI.Core;
using TMI.Mathematics;
using TMI.Number;

namespace TMI.InAppPurchase {

    public class CurrencyIapMultiplier : BaseIapReward {

        private readonly Currency cost;
        private readonly IPlayerProfileProxy playerProfile;

        public CurrencyIapMultiplier(IInitializer initializer, IapStoreData data) : base(initializer, data) {
            SessionManager sessionManager = initializer.GetManager<SessionManager>();
            playerProfile = sessionManager.playerProfile;

            cost = new Currency(CurrencyConstant.cash, data.cost);
        }

        public override string description {
            get {
                return "You will receive " + data.title + ".";
            }
        }

        public override void Apply(PlayerProfile playerProfile) {
            playerProfile.DeductCurrency(cost);
            playerProfile.ApplyIapParameter(data.unlockItemId, new Percent(data.amount));
        }

        public override bool CanAfford() {
            return playerProfile.CanAfford(cost);
        }
    }


}