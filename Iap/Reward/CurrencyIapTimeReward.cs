﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.Number;

namespace TMI.InAppPurchase {

    public class CurrencyIapTimeReward : BaseIapReward {

        private readonly Currency goldEarned;
        private readonly Currency cost;

        private readonly IPlayerProfileProxy playerProfile;

        public override string description {
            get {
                return "You will earn " + FormattingHelper.FormatCurrency(goldEarned) + " gold.";
            }
        }

        public CurrencyIapTimeReward(IInitializer initializer, IapStoreData data) : base(initializer, data) {
            SessionManager sessionManager = initializer.GetManager<SessionManager>();
            playerProfile = sessionManager.playerProfile;

            TimeSpan duration = TimeSpan.FromDays(data.amount);
            this.goldEarned = CurrencyHelper.CalculateGoldEarnedOverTime(initializer, playerProfile, duration);

            cost = new Currency(CurrencyConstant.cash, data.cost);
        }

        public override void Apply(PlayerProfile playerProfile) {
            playerProfile.DeductCurrency(cost);
            playerProfile.AddCurrency(goldEarned);
        }

        public override bool CanAfford() {
            return playerProfile.CanAfford(cost);
        }
    }

}