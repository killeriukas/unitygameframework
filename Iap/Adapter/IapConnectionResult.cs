﻿
namespace TMI.InAppPurchase {

    public class IapConnectionResult : IIapConnectionResult {

        public bool isSuccess { get; private set; }
        public string message { get; private set; }

        public IapConnectionResult(bool isSuccess, string message) {
            this.isSuccess = isSuccess;
            this.message = message;
        }

    }

}