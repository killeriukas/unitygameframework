﻿using System;
using System.Collections.Generic;

namespace TMI.InAppPurchase {

    public interface IIapAdapter {
        void Initialize(Action<IIapConnectionResult> callback, Action<IIapPurchaseResult> onPurchaseFinished);
        bool isConnected { get; }
        void Reconnect();
        IEnumerable<IIapProduct> allProducts { get; }
        void Purchase(string productId);
        void Consume(string productId);
        IIapProduct GetProduct(string productId);
    }

}