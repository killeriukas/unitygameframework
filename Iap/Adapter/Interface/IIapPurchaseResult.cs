﻿
namespace TMI.InAppPurchase {

    public interface IIapPurchaseResult {
        IIapTransaction transaction { get; }
        IIapProduct product { get; }
        bool isSuccess { get; }
    }

}