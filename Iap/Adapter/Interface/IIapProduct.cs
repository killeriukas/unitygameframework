﻿
namespace TMI.InAppPurchase {

    public interface IIapProduct {
        string id { get; }
        string title { get; }
        string description { get; }
        string price { get; }
        string spriteName { get; }
        int amount { get; }
        string amountDescription { get; }
        string currencyCode { get; }
        decimal actualPrice { get; }
    }

}