﻿
namespace TMI.InAppPurchase {

    public interface IIapConnectionResult {
        bool isSuccess { get; }
        string message { get; }
    }

}