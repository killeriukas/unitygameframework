﻿
namespace TMI.InAppPurchase {

    public interface IIapTransaction {
        string id { get; }
        string receipt { get; }
        string signature { get; }
    }

}