﻿#if STANS_ASSETS

using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.Data;

namespace TMI.InAppPurchase {

    public class StanAssetsIapAdapter : IIapAdapter {

        private Action<IIapConnectionResult> onConnectionDone;
        private Action<IIapPurchaseResult> onPurchaseFinished;

        public bool isConnected { get { return UM_InAppPurchaseManager.Client.IsConnected; } }

        private readonly IDataManager dataManager;

        public StanAssetsIapAdapter(IInitializer initializer) {
            dataManager = initializer.GetManager<DataManager, IDataManager>();
        }

        public void Initialize(Action<IIapConnectionResult> onConnectionDone, Action<IIapPurchaseResult> onPurchaseFinished) {
            Assert.IsNull(onConnectionDone, "OnConnectionDone cannot be null.");
            this.onConnectionDone = onConnectionDone;

            Assert.IsNull(onPurchaseFinished, "OnPurchaseFinished cannot be null.");
            this.onPurchaseFinished = onPurchaseFinished;

            UM_InAppPurchaseManager.Client.OnPurchaseFinished += OnPurchaseFinished;

            UM_InAppPurchaseManager.Client.OnServiceConnected += OnServiceConnected;
            UM_InAppPurchaseManager.Client.Connect();
        }

        private void OnPurchaseFinished(UM_PurchaseResult result) {
            IIapPurchaseResult purchaseResult = new StanAssetIapPurchaseResult(result, dataManager);
            onPurchaseFinished(purchaseResult);
        }

        private void OnServiceConnected(UM_BillingConnectionResult result) {
            IapConnectionResult connectionResult = new IapConnectionResult(result.isSuccess, result.message);
            onConnectionDone(connectionResult);
        }

        public void Reconnect() {
            UM_InAppPurchaseManager.Client.Connect();
        }

        public IEnumerable<IIapProduct> allProducts {
            get {
                List<IIapProduct> products = new List<IIapProduct>();
                foreach(UM_InAppProduct sa_product in UM_InAppPurchaseManager.InAppProducts) {
                    StanAssetIapProduct convertedProduct = new StanAssetIapProduct(sa_product, dataManager);
                    products.Add(convertedProduct);
                }
                return products;
            }
        }

        public void Purchase(string productId) {
            UM_InAppPurchaseManager.Client.Purchase(productId);
        }

        public void Consume(string productId) {
            UM_InAppPurchaseManager.Client.Consume(productId);
        }
    }

}

#endif