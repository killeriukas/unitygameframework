﻿#if STANS_ASSETS

using TMI.Data;

namespace TMI.InAppPurchase {

    public class StanAssetIapPurchaseResult : IIapPurchaseResult {

        public bool isSuccess { get; private set; }

        public IIapTransaction transaction { get; private set; }
        public IIapProduct product { get; private set; }

        public StanAssetIapPurchaseResult(UM_PurchaseResult result, IDataManager dataManager) {
            isSuccess = result.isSuccess;

            transaction = new StanAssetIapTransaction(result);
            product = new StanAssetIapProduct(result.product, dataManager);
        }
    }

}

#endif