﻿#if STANS_ASSETS

using TMI.Data;

namespace TMI.InAppPurchase {

    public class StanAssetIapProduct : IIapProduct {

        public string id { get; private set; }
        public string title { get; private set; }
        public string description { get; private set; }
        public string price { get; private set; }
        public int amount { get; private set; }
        public string amountDescription { get; private set; }
        public string spriteName { get; private set; }
        public string currencyCode { get; private set; }
        public decimal actualPrice { get; private set; }

        public StanAssetIapProduct(UM_InAppProduct product, IDataManager dataManager) {
            id = product.id;
            title = product.DisplayName;
            price = product.LocalizedPrice;
            description = product.Description;
            currencyCode = product.CurrencyCode;
            actualPrice = (decimal)product.ActualPriceValue;

            IapStoreData data = dataManager.GetDataById<IapStoreData>(id);
            spriteName = data.iconName;
            amount = data.amount;
            amountDescription = data.amountPrefix + data.amount;
        }
    }

}

#endif