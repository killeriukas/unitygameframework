﻿#if STANS_ASSETS

namespace TMI.InAppPurchase {

    public class StanAssetIapTransaction : IIapTransaction {

        public string id { get; private set; }
        public string receipt { get; private set; }
        public string signature { get; private set; }

        public StanAssetIapTransaction(UM_PurchaseResult result) {
            id = result.TransactionId;

#if UNITY_EDITOR
            signature = null;
            receipt = null;
#else
#if UNITY_ANDROID
            signature = result.Google_PurchaseInfo.Signature;
            receipt = result.Google_PurchaseInfo.OriginalJson;
#elif UNITY_IOS
            receipt = result.IOS_PurchaseInfo.Receipt;
            signature = null;
#else
            signature = null;
#endif
#endif
        }
    }
}

#endif