﻿using TMI.Data;
using UnityEngine.Purchasing;

namespace TMI.InAppPurchase {

    public class UnityDefaultIapPurchaseResult : IIapPurchaseResult {

        public bool isSuccess { get; private set; }

        public IIapTransaction transaction { get; private set; }
        public IIapProduct product { get; private set; }

        public UnityDefaultIapPurchaseResult(Product product, IDataManager dataManager, bool isSuccess) {
            this.isSuccess = isSuccess;

            this.transaction = new UnityDefaultIapTransaction(product);
            this.product = new UnityDefaultIapProduct(product, dataManager);
        }
    }

}