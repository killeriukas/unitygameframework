﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.Data;
using TMI.LogManagement;
using UnityEngine;
using UnityEngine.Purchasing;

namespace TMI.InAppPurchase {

    public class UnityDefaultIapAdapter : IIapAdapter, IStoreListener, ILoggable {

        private Action<IIapConnectionResult> onConnectionDone;
        private Action<IIapPurchaseResult> onPurchaseFinished;

        public bool isConnected { get; private set; }

        private readonly ConfigurationBuilder configurationModule;
        private IStoreController storeController;
        private IExtensionProvider extensionProvider;

        private readonly IDataManager dataManager;

        public UnityDefaultIapAdapter(IInitializer initializer) {
            dataManager = initializer.GetManager<DataManager, IDataManager>();

            StandardPurchasingModule purchasingModule = StandardPurchasingModule.Instance();
            configurationModule = ConfigurationBuilder.Instance(purchasingModule);
        }

        public void AddProduct(string productId, ProductType productType) {
            configurationModule.AddProduct(productId, productType);
        }

        public void Initialize(Action<IIapConnectionResult> onConnectionDone, Action<IIapPurchaseResult> onPurchaseFinished) {
            Assert.IsNull(onConnectionDone, "OnConnectionDone cannot be null.");
            this.onConnectionDone = onConnectionDone;

            Assert.IsNull(onPurchaseFinished, "OnPurchaseFinished cannot be null.");
            this.onPurchaseFinished = onPurchaseFinished;

            UnityPurchasing.Initialize(this, configurationModule);
        }

        public void Reconnect() {
            UnityPurchasing.Initialize(this, configurationModule);
        }

        public IEnumerable<IIapProduct> allProducts {
            get {
                List<IIapProduct> products = new List<IIapProduct>();
                if(isConnected) {
                    foreach(Product product in storeController.products.all) {
                        IapStoreData iapStoreData = dataManager.GetDataById<IapStoreData>(product.definition.id);
                        if(iapStoreData.enabled && product.availableToPurchase) {
                            UnityDefaultIapProduct convertedProduct = new UnityDefaultIapProduct(product, dataManager);
                            products.Add(convertedProduct);
                        }
                    }
                } else {
                    foreach(ProductDefinition productDefinition in configurationModule.products) {
                        IapStoreData iapStoreData = dataManager.GetDataById<IapStoreData>(productDefinition.id);
                        if(iapStoreData.enabled) {
                            UnityDefaultIapProduct convertedProduct = new UnityDefaultIapProduct(productDefinition.id, dataManager);
                            products.Add(convertedProduct);
                        }
                    }
                }
                return products;
            }
        }

        public void Purchase(string productId) {
            storeController.InitiatePurchase(productId);
        }

        public void Consume(string productId) {
            //no need to consume, because if it crashed before this, it would not have returned as PurchaseProcessingResult.Complete anyway
            //implement this only once the cloud save is in the game
        }

        public void OnInitializeFailed(InitializationFailureReason error) {
            isConnected = false;

            IapConnectionResult connectionResult = new IapConnectionResult(false, error.ToString());
            onConnectionDone(connectionResult);
        }

        //purchase was successful
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) {
            UnityDefaultIapPurchaseResult purchaseResult = new UnityDefaultIapPurchaseResult(e.purchasedProduct, dataManager, true);
            onPurchaseFinished(purchaseResult);
            return PurchaseProcessingResult.Complete;
        }

        //purchase failed for any reason
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p) {
            Logging.LogWarning(this, "IAP purchase [{0}] has failed due to [{1}].", i.definition.id, p.ToString());
            UnityDefaultIapPurchaseResult purchaseResult = new UnityDefaultIapPurchaseResult(i, dataManager, false);
            onPurchaseFinished(purchaseResult);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
            storeController = controller;
            extensionProvider = extensions;

            isConnected = true;

            IapConnectionResult connectionResult = new IapConnectionResult(true, "Connection successful!");
            onConnectionDone(connectionResult);
        }

        public IIapProduct GetProduct(string productId) {
            if(isConnected) {
                Product product = storeController.products.WithID(productId);
                UnityDefaultIapProduct convertedProduct = new UnityDefaultIapProduct(product, dataManager);
                return convertedProduct;
            } else {
                foreach(ProductDefinition productDefinition in configurationModule.products) {
                    if(productId.Equals(productDefinition.id)) {
                        UnityDefaultIapProduct convertedProduct = new UnityDefaultIapProduct(productDefinition.id, dataManager);
                        return convertedProduct;
                    }
                }
                throw new ArgumentException("Product id was not found: " + productId);
            }
        }
    }

}