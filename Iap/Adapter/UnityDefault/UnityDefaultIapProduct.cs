﻿using TMI.Data;
using TMI.Helper;
using UnityEngine.Purchasing;

namespace TMI.InAppPurchase {

    public class UnityDefaultIapProduct : IIapProduct {

        public string id { get; private set; }
        public string title { get; private set; }
        public string description { get; private set; }
        public string price { get; private set; }
        public int amount { get; private set; }
        public string amountDescription { get; private set; }
        public string spriteName { get; private set; }
        public string currencyCode { get; private set; }
        public decimal actualPrice { get; private set; }

        public UnityDefaultIapProduct(Product product, IDataManager dataManager) {
            ProductMetadata metadata = product.metadata;

            id = product.definition.id;
            //title = metadata.localizedTitle;
            price = metadata.localizedPriceString;
            description = metadata.localizedDescription;
            currencyCode = metadata.isoCurrencyCode;
            actualPrice = metadata.localizedPrice;

            IapStoreData data = dataManager.GetDataById<IapStoreData>(id);
            title = data.title;
            spriteName = data.iconName;
            amount = data.amount;
            amountDescription = data.amountPrefix + FormattingHelper.FormatDouble(data.amount);
        }

        public UnityDefaultIapProduct(string productId, IDataManager dataManager) {
            id = productId;
            //title = metadata.localizedTitle;
            price = "N/A";
            description = "No internet";
            currencyCode = "";
            actualPrice = 0;

            IapStoreData data = dataManager.GetDataById<IapStoreData>(id);
            title = data.title;
            spriteName = data.iconName;
            amount = data.amount;
            amountDescription = data.amountPrefix + FormattingHelper.FormatDouble(data.amount);
        }

    }

}