﻿using System.Collections.Generic;
using UnityEngine.Purchasing;

namespace TMI.InAppPurchase {

    public class UnityDefaultIapTransaction : IIapTransaction {

        public string id { get; private set; }
        public string receipt { get; private set; }
        public string signature { get; private set; }

        public UnityDefaultIapTransaction(Product product) {
            id = product.transactionID;

            //INFO: copy paste from Tenjin website
            Dictionary<string, object> wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(product.receipt);
            if(null == wrapper) {
                return;
            }

            string payload = (string)wrapper["Payload"]; // For Apple this will be the base64 encoded ASN.1 receipt

#if UNITY_EDITOR
            signature = null;
            receipt = null;
#else
#if UNITY_ANDROID
            Dictionary<string, object> gpDetails = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
            receipt = (string)gpDetails["json"];
            signature = (string)gpDetails["signature"];
#elif UNITY_IOS
                        receipt = payload;
                        signature = null;
#else
                        signature = null;
#endif
#endif
            //ENDS: copy paste from Tenjin website
        }
    }
}