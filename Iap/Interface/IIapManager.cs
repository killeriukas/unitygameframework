﻿using System;
using System.Collections.Generic;
using TMI.Core;

namespace TMI.InAppPurchase {

    public interface IIapManager : IManager {
        void Initialize(IIapAdapter iapAdapter);
        void Purchase(string productId);
        void Consume(string productId);
        IapManager.State currentState { get; }
        IEnumerable<IIapProduct> allProducts { get; }
        event Action<IIapConnectionResult> onInitialized;
        event Action<IIapPurchaseResult> onPurchaseCompleted;
        void Reconnect();
        IIapProduct GetProduct(string productId);
    }


}