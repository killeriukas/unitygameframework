﻿using Newtonsoft.Json;
using TMI.RewardManagement;

namespace TMI.InAppPurchase {

    [JsonObject(MemberSerialization.OptIn)]
    public class IapStoreData : BaseRewardDescriptionData {

        [JsonProperty("Enabled")]
        public bool enabled { get; private set; }

        [JsonProperty("Category Id")]
        public string categoryId { get; private set; }

        [JsonProperty("Cost")]
        public int cost { get; private set; }

        [JsonProperty("Amount Prefix")]
        public string amountPrefix { get; private set; }

    }

}