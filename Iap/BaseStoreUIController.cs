﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMI.AnalyticsManagement;
using TMI.AssetManagement;
using TMI.Core;
using TMI.Helper;
using TMI.UI;
using TMI.UI.Extension;
using TMI.UI.Popup;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.U2D;

namespace TMI.InAppPurchase {

    public abstract class BaseStoreUIController : BaseClosableUIController, IAssetGroupInitialization {

        [SerializeField]
        private UIList verticalCategoryList;

        protected IAnalyticsManager analyticsManager { get; private set; }
        protected IIapManager iapManager { get; private set; }

        private readonly Dictionary<string, Cell> iapCellById = new Dictionary<string, Cell>();

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            iapManager = initializer.GetManager<IapManager, IIapManager>();
            analyticsManager = initializer.GetManager<GameAnalyticsManager, IAnalyticsManager>();
        }

        protected void CreateInAppPurchasesGroup() {
            Assert.IsTrue<InvalidOperationException>(iapManager.currentState == IapManager.State.Uninitialized, "Cannot create IAP group, when IapManager is uninitialized!");
            Assert.IsTrue<InvalidOperationException>(iapCellById.Count > 0, "Cannot initialize IAP group twice!");

            IEnumerable<IIapProduct> iapProducts = iapManager.allProducts;
            if(iapProducts.Count() > 0) {
                Cell iapCategoryCell = CreateCategoryCell("Cash");

                UIList itemList = iapCategoryCell.FindComponent<UIList>("item_list");

                foreach(IIapProduct product in iapProducts) {
                    Cell iapCell = itemList.CloneCell();
                    iapCellById.Add(product.id, iapCell);

                    RefreshCell(product);
                }

            }

            if(iapManager.currentState == IapManager.State.PendingResponse) {
                iapManager.onInitialized += OnIapInitialized;
            }
        }

        protected Cell CreateCategoryCell(string categoryName) {
            Cell cell = verticalCategoryList.CloneCell();
            UITextPro category = cell.FindComponent<UITextPro>("category_name");
            category.text = categoryName;
            return cell;
        }

        private void PurchaseProduct(string productId) {
            LoadingScreenUIController loadingScreenUIController = uiManager.LoadUI<LoadingScreenUIController>();
            loadingScreenUIController.Show();

            iapManager.onPurchaseCompleted += OnPurchaseCompleted;
            iapManager.Purchase(productId);
        }

        protected virtual void OnPurchaseCompleted(IIapPurchaseResult result) {
            iapManager.onPurchaseCompleted -= OnPurchaseCompleted;

            LoadingScreenUIController loadingScreenUIController = uiManager.LoadUI<LoadingScreenUIController>(false);
            loadingScreenUIController.Hide();

            InformationPopupUIController informationPopupUIController = uiManager.LoadUI<InformationPopupUIController>();
            informationPopupUIController.popupName = "Information";

            if(result.isSuccess) {
                informationPopupUIController.popupDescription = "You have successfully purchased the " + result.product.title + ".";
            } else {
                informationPopupUIController.popupDescription = "Something went wrong with the purchase. Please try again later.";
            }

            informationPopupUIController.Show();
        }

        private void OnIapButtonClicked(IIapProduct product) {
            OnIapButtonClicked(product.id, product.title, product.description);
        }

        protected void OnIapButtonClicked(string productId, string title, string description) {
            GenericAnalyticsTrackerEvent iapButtonTrackerEvent = AnalyticsHelper.CreateButtonClickEvent(sceneName, productId);
            analyticsManager.LogEvent(iapButtonTrackerEvent);

            if(iapManager.currentState == IapManager.State.Initialized) {
                ConfirmationPopupUIController confirmationPopupUIController = uiManager.LoadUI<ConfirmationPopupUIController>();
                confirmationPopupUIController.popupName = title;
                confirmationPopupUIController.popupDescription = description;
                confirmationPopupUIController.confirmButtonName = "Buy";
                confirmationPopupUIController.dismissButtonName = "Cancel";

                Action<PointerEventData> onBuyConfirmClicked = (data) => {
                    GenericAnalyticsTrackerEvent confirmButtonTrackerEvent = AnalyticsHelper.CreateButtonClickEvent(sceneName, productId + "_confirm");
                    analyticsManager.LogEvent(confirmButtonTrackerEvent);

                    ConfirmationPopupUIController confirmationPopup = uiManager.LoadUI<ConfirmationPopupUIController>(false);
                    confirmationPopup.Hide();

                    PurchaseProduct(productId);
                };

                confirmationPopupUIController.onConfirmButtonClick += onBuyConfirmClicked;
                confirmationPopupUIController.Show();
            } else {
                InformationPopupUIController informationPopupUIController = uiManager.LoadUI<InformationPopupUIController>();
                informationPopupUIController.popupName = "Information";
                informationPopupUIController.popupDescription = "The store is unavailable at the moment. Please, check the internet connection and come back later.";
                informationPopupUIController.Show();
            }
        }

        private void RefreshCell(IIapProduct product) {
            Cell iapCell = iapCellById[product.id];

            UITextPro title = iapCell.FindComponent<UITextPro>("item_name");
            title.text = product.title;

            UITextPro price = iapCell.FindComponent<UITextPro>("item_price");
            price.text = product.price;

            UITextPro amount = iapCell.FindComponent<UITextPro>("item_amount");
            amount.text = product.amountDescription;

            UIButton iapButton = iapCell.FindComponent<UIButton>("item_button");

            Action<PointerEventData> onClick = (data) => {
                OnIapButtonClicked(product);
            };

            iapButton.ClearOnButtonClick();
            iapButton.onButtonClick += onClick;
        }

        private void OnIapInitialized(IIapConnectionResult result) {
            iapManager.onInitialized -= OnIapInitialized;

            //failed to load again, reconnect will be allowed
            if(result.isSuccess) {
                foreach(var kvp in iapCellById) {
                    kvp.Value.gameObject.SetActive(false);
                }

                IEnumerable<IIapProduct> iapProducts = iapManager.allProducts;
                foreach(IIapProduct product in iapProducts) {
                    RefreshCell(product);
                    Cell cell = iapCellById[product.id];
                    cell.gameObject.SetActive(true);
                }
            }

            //LoadingScreenUIController loadingScreenUIController = uiManager.LoadUI<LoadingScreenUIController>(false);
            //loadingScreenUIController.Hide();

         //   base.Show();
        }

        public override void Show() {
            if(iapManager.currentState == IapManager.State.ReconnectAllowed) {
                iapManager.onInitialized += OnIapInitialized;
                iapManager.Reconnect();
            }

            base.Show();
            //if(iapManager.currentState != IapManager.State.Initialized) {
            //    iapManager.onInitialized += OnIapInitialized;

            //    Assert.IsTrue<InvalidOperationException>(iapManager.currentState == IapManager.State.Uninitialized, "Initialize the IapManager before calling for StoreUIController.Show().");
            //    if(iapManager.currentState == IapManager.State.ReconnectAllowed) {
            //        iapManager.Reconnect();
            //    }

            //    LoadingScreenUIController loadingScreenUIController = uiManager.LoadUI<LoadingScreenUIController>();
            //    loadingScreenUIController.Show();
            //} else {
            //    base.Show();
            //}
        }

        public virtual void InitializeAssets(IAssetGroupComplete assetGroup) {
            Assert.IsTrue<InvalidOperationException>(iapManager.currentState == IapManager.State.Uninitialized, "Cannot initialize IAP assets, when IapManager is uninitialized!");

            SpriteAtlas iapSpriteAtlas = assetGroup.GetAsset<SpriteAtlas>(SpriteAtlasConstants.iapStoreIconAtlas);

            IEnumerable<IIapProduct> iapProducts = iapManager.allProducts;
            foreach(IIapProduct product in iapProducts) {
                //this one will crash if CreateInAppPurchasesGroup() is not called beforehand
                Cell iapCell = iapCellById[product.id];

                UIImage iapImage = iapCell.FindComponent<UIImage>("item_image");
                iapImage.sprite = iapSpriteAtlas.GetSprite(product.spriteName);
            }

        }

        protected override void OnDestroy() {
            if(iapManager.currentState == IapManager.State.PendingResponse) {
                iapManager.onInitialized -= OnIapInitialized;
            }
            base.OnDestroy();
        }
    }

}