﻿using TMI.Notification;

namespace TMI.InAppPurchase {

    public class IapProductPurchaseCompletedNotification : BaseValueNotification<IIapPurchaseResult> {

        public IapProductPurchaseCompletedNotification(IIapPurchaseResult product) : base(product) {
        }

    }


}

