﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.Helper;
using TMI.LogManagement;

namespace TMI.InAppPurchase {

    public class IapManager : BaseNotificationManager, IIapManager {

        public enum State {
            Uninitialized,
            PendingResponse,
            ReconnectAllowed,
            Initialized
        }

        public State currentState { get; private set; }

        public event Action<IIapConnectionResult> onInitialized;
        public event Action<IIapPurchaseResult> onPurchaseCompleted;

        private IIapAdapter iapAdapter;

        protected override void Awake() {
            base.Awake();
            currentState = State.Uninitialized;
        }

        public void Initialize(IIapAdapter iapAdapter) {
            Assert.IsTrue<InvalidOperationException>(currentState != State.Uninitialized, "Cannot initialize more than once!");

            Logging.Log(this, "Initializing the iap manager with a new adapter.");
            this.iapAdapter = iapAdapter;
            this.currentState = State.PendingResponse;
            iapAdapter.Initialize(OnInitialized, OnPurchaseFinished);
        }

        private void OnInitialized(IIapConnectionResult result) {
            Logging.Log(this, "Iap initialized with a result of [{0}] and a message [{1}].", result.isSuccess, result.message);

            if(result.isSuccess) {
                currentState = State.Initialized;
            } else {
                currentState = State.ReconnectAllowed;
            }

            GeneralHelper.CallNullCheck(onInitialized, result);
        }

        private void OnPurchaseFinished(IIapPurchaseResult result) {
            GeneralHelper.CallNullCheck(onPurchaseCompleted, result);
            Trigger(new IapProductPurchaseCompletedNotification(result));
        }

        public IEnumerable<IIapProduct> allProducts { get { return iapAdapter.allProducts; } }

        public void Reconnect() {
            Assert.IsTrue<InvalidOperationException>(currentState != State.ReconnectAllowed, "Cannot reconnect when current state is not ReconnectAllowed. Current state: " + currentState);
            currentState = State.PendingResponse;
            iapAdapter.Reconnect();
        }

        public void Purchase(string productId) {
            Logging.Log(this, "Purchasing item [{0}].", productId);
            iapAdapter.Purchase(productId);
        }

        public void Consume(string productId) {
            Logging.Log(this, "Consuming item [{0}].", productId);
            iapAdapter.Consume(productId);
        }

        public IIapProduct GetProduct(string productId) {
            Logging.Log(this, "Getting item [{0}].", productId);
            return iapAdapter.GetProduct(productId);
        }
    }

}