﻿using TMI.AnalyticsManagement;

namespace TMI.Helper {

    public static class AnalyticsHelper {

        public static GenericAnalyticsTrackerEvent CreateButtonClickEvent(string screenId, string buttonId) {
            GenericAnalyticsTrackerEvent trackerEvent = new GenericAnalyticsTrackerEvent();
            trackerEvent.id = "button_click";
            trackerEvent.data.Add("screen", screenId);
            trackerEvent.data.Add("button_id", buttonId);
            return trackerEvent;
        }

        public static GenericAnalyticsTrackerEvent CreateShowScreenEvent(string screenId) {
            GenericAnalyticsTrackerEvent trackerEvent = new GenericAnalyticsTrackerEvent();
            trackerEvent.id = "show_screen";
            trackerEvent.data.Add("screen", screenId);
            return trackerEvent;
        }

        public static GenericAnalyticsTrackerEvent CreateHideScreenEvent(string screenId) {
            GenericAnalyticsTrackerEvent trackerEvent = new GenericAnalyticsTrackerEvent();
            trackerEvent.id = "hide_screen";
            trackerEvent.data.Add("screen", screenId);
            return trackerEvent;
        }

        public static GenericAnalyticsTrackerEvent CreateOnboardingEvent(string typeId, string eventId) {
            GenericAnalyticsTrackerEvent trackerEvent = new GenericAnalyticsTrackerEvent();
            trackerEvent.id = "onboarding_event";
            trackerEvent.data.Add("type", typeId); //start, continue or finish
            trackerEvent.data.Add("id", eventId);
            return trackerEvent;
        }

    }


}