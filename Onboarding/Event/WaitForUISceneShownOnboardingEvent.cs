﻿using Newtonsoft.Json;
using TMI.OnboardingManagement;
using TMI.UI;

public class WaitForUISceneShownOnboardingEvent : BaseOnboardingEvent {

    [JsonProperty]
    private string sceneName;

    protected WaitForUISceneShownOnboardingEvent() {
    }

    public WaitForUISceneShownOnboardingEvent(string sceneName, string id) : base(id) {
        this.sceneName = sceneName;
    }

    protected override void StartOnboardingEventInternal() {
        Listen<UISceneShownNotification>(this, OnUISceneShown);
    }

    private void OnUISceneShown(UISceneShownNotification notification) {
        if(notification.IsMyNotification(sceneName)) {
            FinishOnboardingEvent();
        }
    }

}
