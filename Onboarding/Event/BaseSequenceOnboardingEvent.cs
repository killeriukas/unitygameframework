﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public abstract class BaseSequenceOnboardingEvent : BaseOnboardingEvent, ISequenceOnboarding {

        [JsonProperty]
        private List<IOnboardingEvent> eventList = new List<IOnboardingEvent>();

        protected Queue<IOnboardingEvent> currentEventQueue { get; private set; }

        protected BaseSequenceOnboardingEvent() {
        }

        public BaseSequenceOnboardingEvent(string id) : base(id) {
            currentEventQueue = new Queue<IOnboardingEvent>();
        }

        [OnSerializing]
        private void OnSerializing(StreamingContext context) {
            eventList = new List<IOnboardingEvent>(currentEventQueue);
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context) {
            currentEventQueue = new Queue<IOnboardingEvent>(eventList);
        }

        public void EnqueueEvent(IOnboardingEvent onboardingEvent) {
            eventList.Add(onboardingEvent);
            currentEventQueue.Enqueue(onboardingEvent);
        }

        public abstract void FinishOnboardingEvent(IOnboardingEvent finishedEvent);

        public override void Upgrade(IOnboardingEvent oldOnboardingEvent) {
            base.Upgrade(oldOnboardingEvent);

            BaseSequenceOnboardingEvent oldOnboardingEventCast = (BaseSequenceOnboardingEvent)oldOnboardingEvent;

            foreach(IOnboardingEvent old in oldOnboardingEventCast.eventList) {
                IOnboardingEvent foundEvent = eventList.Find(x => x.id.Equals(old.id));
                foundEvent.Upgrade(old);
            }

            currentEventQueue = new Queue<IOnboardingEvent>(eventList);
        }

        protected void RemoveEventForUpgrade(string completedEventId) {
            eventList.RemoveAll(x => x.id.Equals(completedEventId));
        }

    }

}


