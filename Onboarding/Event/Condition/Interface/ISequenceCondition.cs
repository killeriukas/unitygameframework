﻿using TMI.Core;

namespace TMI.OnboardingManagement {

    public interface ISequenceCondition {
        bool CanFinishSequence();
        void Setup(IInitializer initializer);
    }

}