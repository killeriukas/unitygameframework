﻿using Newtonsoft.Json;
using System;
using TMI.AnalyticsManagement;
using TMI.Core;
using TMI.Helper;
using TMI.LogManagement;
using TMI.Notification;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    [LoggableGroup("OnboardingEvent")]
    public abstract class BaseOnboardingEvent : IOnboardingEvent, INotificationListener {

        [JsonProperty]
        public string id { get; private set; }

        protected IInitializer initializer { get; private set; }

        private ISequenceOnboarding sequenceOnboarding;
        private INotificationManager notificationManager;
        private IAnalyticsManager analyticsManager;

        protected BaseOnboardingEvent() {
        }

        protected BaseOnboardingEvent(string id) {
            this.id = id;
        }

        public virtual void Setup(IInitializer initializer, ISequenceOnboarding onboarding) {
            this.initializer = initializer;
            this.sequenceOnboarding = onboarding;
            this.analyticsManager = initializer.GetManager<GameAnalyticsManager, IAnalyticsManager>();
            this.notificationManager = initializer.GetManager<NotificationManager, INotificationManager>();
        }

        public void StartOnboardingEvent() {
            GenericAnalyticsTrackerEvent trackerEvent = AnalyticsHelper.CreateOnboardingEvent("start", id);
            analyticsManager.LogEvent(trackerEvent);

            Logging.Log(this, "Starting onboarding event [{0}].", id);
            StartOnboardingEventInternal();
        }

        protected abstract void StartOnboardingEventInternal();

        public void ContinueOnboardingEvent(bool save, string eventId) {
            GenericAnalyticsTrackerEvent trackerEvent = AnalyticsHelper.CreateOnboardingEvent("continue", id);
            analyticsManager.LogEvent(trackerEvent);

            Logging.Log(this, "Continue (saving) onboarding event [{0}] for id [{1}].", id, eventId);
            sequenceOnboarding.ContinueOnboardingEvent(save, eventId);
        }

        protected void FinishOnboardingEvent() {
            GenericAnalyticsTrackerEvent trackerEvent = AnalyticsHelper.CreateOnboardingEvent("finish", id);
            analyticsManager.LogEvent(trackerEvent);

            Logging.Log(this, "Finishing onboarding event [{0}].", id);
            sequenceOnboarding.FinishOnboardingEvent(this);
        }

        protected void Listen<TNotification>(INotificationListener listener, Action<TNotification> callback) where TNotification : INotification {
            notificationManager.Listen<TNotification>(listener, callback);
        }

        protected void Listen<TNotification, TPredicate>(INotificationListener listener,
            Action<TNotification> callback,
            Func<TPredicate, bool> predicate)
            where TNotification : BasePredicateNotification
            where TPredicate : BasePredicateNotification {
            notificationManager.Listen<TNotification, TPredicate>(listener, callback, predicate);
        }

        public virtual void Dispose() {
            notificationManager.StopListenAll(this);
        }

        public virtual void Upgrade(IOnboardingEvent oldOnboardingEvent) {
            //do nothing here
        }
    }

}


