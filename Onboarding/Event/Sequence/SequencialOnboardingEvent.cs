﻿using Newtonsoft.Json;
using System;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class SequencialOnboardingEvent : BaseSequenceOnboardingEvent {

        protected SequencialOnboardingEvent() {
        }

        public SequencialOnboardingEvent(string id) : base(id) {
        }

        protected override void StartOnboardingEventInternal() {
            IOnboardingEvent currentEvent = currentEventQueue.Peek();
            currentEvent.Setup(initializer, this);
            currentEvent.StartOnboardingEvent();
        }

        public override void FinishOnboardingEvent(IOnboardingEvent finishedEvent) {
            string finishedEventId = finishedEvent.id;

            //dispose of the old pending event, which has to be the finished event
            IOnboardingEvent oldOnboardingEvent = currentEventQueue.Dequeue();

            Assert.IsFalse<ArgumentException>(finishedEventId.Equals(oldOnboardingEvent.id),
                "Cannot finish event with an id different to the current event. Finished Id: " + finishedEventId + " Current Id: " + oldOnboardingEvent.id);

            finishedEvent.Dispose();

            if(currentEventQueue.Count > 0) {
                StartOnboardingEventInternal();
            } else {
                FinishOnboardingEvent();
            }
        }

    }


}


