﻿using Newtonsoft.Json;
using TMI.Core;
using TMI.LogManagement;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class ConditionalSequencialOnboardingEvent : SequencialOnboardingEvent {

        [JsonProperty]
        private ISequenceCondition condition;

        //sequence doesn't save until it finishes
        private bool wasConditionTested = false;
        private bool canFinishSequence = false;

        private ConditionalSequencialOnboardingEvent() {
        }

        public ConditionalSequencialOnboardingEvent(ISequenceCondition condition, string id) : base(id) {
            this.condition = condition;
        }

        public override void Setup(IInitializer initializer, ISequenceOnboarding onboarding) {
            base.Setup(initializer, onboarding);
            condition.Setup(initializer);
        }

        protected override void StartOnboardingEventInternal() {
            Logging.Log(this, "Starting conditional sequence onboarding event with wasConditionTested=[{0}].", wasConditionTested);
            if(!wasConditionTested) {
                wasConditionTested = true;
                canFinishSequence = condition.CanFinishSequence();
            }

            Logging.Log(this, "Conditional sequence onboarding event canFinishSequence=[{0}].", canFinishSequence);
            if(canFinishSequence) {
                FinishOnboardingEvent();
            } else {
                base.StartOnboardingEventInternal();
            }
        }

    }

}


