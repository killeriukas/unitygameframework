﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TMI.LogManagement;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class ParallelOnboardingEvent : BaseOnboardingEvent {

        //TODO: this one doesn't work!!!!!!!!!!!!!!
        //NEED TO FIX IT

        [JsonProperty]
        private HashSet<IOnboardingEvent> activeEvents = new HashSet<IOnboardingEvent>();

        [JsonProperty]
        private List<string> completedEvents = new List<string>();

        protected ParallelOnboardingEvent() {
        }

        public ParallelOnboardingEvent(string id) : base(id) {
        }

        public void EnqueueEvent(IOnboardingEvent onboardingEvent) {
            activeEvents.Add(onboardingEvent);
        }

        protected override void StartOnboardingEventInternal() {
            throw new System.NotImplementedException();

            //foreach(IOnboardingEvent currentEvent in activeEvents) {
            //    Logging.Log(this, "Starting parallel event with id [{0}].", currentEvent.id);
            //    currentEvent.Setup(initializer, this);
            //    currentEvent.StartOnboardingEvent();
            //}
        }

        public void FinishCurrentOnboardingEvent(IOnboardingEvent finishedEvent) {
            //finishedEvent.Dispose();

            //activeEvents.Remove(finishedEvent);
            //completedEvents.Add(finishedEvent.id);

            //if(0 == activeEvents.Count) {
            //    base.FinishOnboardingEvent();
            //}
        }

    }


}


