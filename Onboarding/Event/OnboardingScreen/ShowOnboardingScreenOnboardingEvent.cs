﻿using Newtonsoft.Json;
using TMI.UI;

public class ShowOnboardingScreenOnboardingEvent : BaseOnboardingScreenOnboardingEvent {

    [JsonProperty]
    private bool showScreen;

    protected ShowOnboardingScreenOnboardingEvent() {
    }

    public ShowOnboardingScreenOnboardingEvent(bool show, string id) : base(id) {
        this.showScreen = show;
    }

    protected override void StartOnboardingEventInternal() {
        if(showScreen) {
            OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(true);
            onboardingScreen.Show();
        } else {
            OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);
            onboardingScreen.Hide();
        }

        FinishOnboardingEvent();
    }

}
