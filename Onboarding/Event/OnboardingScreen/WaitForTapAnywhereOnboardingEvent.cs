﻿using TMI.UI;
using UnityEngine.EventSystems;

public class WaitForTapAnywhereOnboardingEvent : BaseOnboardingScreenOnboardingEvent {

    protected WaitForTapAnywhereOnboardingEvent() {
    }

    public WaitForTapAnywhereOnboardingEvent(string id) : base(id) {
        
    }

    protected override void StartOnboardingEventInternal() {
        OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);
        onboardingScreen.onTapAnywhere += OnTapAnywhere;
    }

    private void OnTapAnywhere(PointerEventData data) {
        OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);
        onboardingScreen.onTapAnywhere -= OnTapAnywhere;

        FinishOnboardingEvent();
    }
}
