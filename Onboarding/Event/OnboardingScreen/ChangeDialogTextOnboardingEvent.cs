﻿using Newtonsoft.Json;
using TMI.UI;

public class ChangeDialogTextOnboardingEvent : BaseOnboardingScreenOnboardingEvent {

    [JsonProperty]
    private string dialogText;

    [JsonProperty]
    private bool hasContinueText;

    protected ChangeDialogTextOnboardingEvent() {
    }

    public ChangeDialogTextOnboardingEvent(string dialogText, bool hasContinueText, string id) : base(id) {
        this.dialogText = dialogText;
        this.hasContinueText = hasContinueText;
    }

    protected override void StartOnboardingEventInternal() {
        OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);
        onboardingScreen.SetDialogText(dialogText, hasContinueText);

        FinishOnboardingEvent();
    }

}
