﻿using Newtonsoft.Json;
using TMI.UI;

public class EnableTargetAreaOnboardingEvent : BaseOnboardingScreenOnboardingEvent {

    [JsonProperty]
    private bool enableTargetArea;

    protected EnableTargetAreaOnboardingEvent() {
    }

    public EnableTargetAreaOnboardingEvent(bool enableTargetArea, string id) : base(id) {
        this.enableTargetArea = enableTargetArea;
    }

    protected override void StartOnboardingEventInternal() {
        OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);
        onboardingScreen.targetAreaEnabled = enableTargetArea;

        FinishOnboardingEvent();
    }

}
