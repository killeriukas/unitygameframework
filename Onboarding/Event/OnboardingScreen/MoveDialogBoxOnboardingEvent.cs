﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TMI.UI;

public class MoveDialogBoxOnboardingEvent : BaseOnboardingScreenOnboardingEvent {

    [JsonProperty]
    [JsonConverter(typeof(StringEnumConverter))]
    private OnboardingScreenUIController.DialogBoxPosition dialogBoxPosition;

    protected MoveDialogBoxOnboardingEvent() {
    }

    public MoveDialogBoxOnboardingEvent(OnboardingScreenUIController.DialogBoxPosition dialogBoxPosition, string id) : base(id) {
        this.dialogBoxPosition = dialogBoxPosition;
    }

    protected override void StartOnboardingEventInternal() {
        OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);
        onboardingScreen.SetDialogBox(dialogBoxPosition);

        FinishOnboardingEvent();
    }

}
