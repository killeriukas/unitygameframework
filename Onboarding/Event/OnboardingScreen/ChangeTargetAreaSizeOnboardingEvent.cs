﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TMI.Core;
using TMI.OnboardingManagement;
using TMI.UI;

public class ChangeTargetAreaPropertiesOnboardingEvent : BaseOnboardingScreenOnboardingEvent {

    [JsonProperty]
    private string locatorId;

    [JsonProperty]
    [JsonConverter(typeof(StringEnumConverter))]
    private OnboardingScreenUIController.ArrowPosition arrowPosition;

    private IOnboardingManager onboardingManager;

    protected ChangeTargetAreaPropertiesOnboardingEvent() {
    }

    public ChangeTargetAreaPropertiesOnboardingEvent(string locatorId, OnboardingScreenUIController.ArrowPosition arrowPosition, string id) : base(id) {
        this.locatorId = locatorId;
        this.arrowPosition = arrowPosition;
    }

    public override void Setup(IInitializer initializer, ISequenceOnboarding onboarding) {
        base.Setup(initializer, onboarding);
        onboardingManager = initializer.GetManager<IOnboardingManager>();
    }

    protected override void StartOnboardingEventInternal() {
        OnboardingScreenUIController onboardingScreen = GetOnboardingScreenUIController(false);

        IUIOnboardingItemLocator itemLocator = onboardingManager.GetOnboardingItemLocator(locatorId);
        onboardingScreen.SetCursorPositionAndSize(itemLocator, arrowPosition);

        FinishOnboardingEvent();
    }

}
