﻿using TMI.Core;
using TMI.OnboardingManagement;
using TMI.UI;

public abstract class BaseOnboardingScreenOnboardingEvent : BaseOnboardingEvent {

    private IUIManager uiManager;

    protected BaseOnboardingScreenOnboardingEvent() {
    }

    protected BaseOnboardingScreenOnboardingEvent(string id) : base(id) {
    }

    public override void Setup(IInitializer initializer, ISequenceOnboarding onboarding) {
        base.Setup(initializer, onboarding);
        uiManager = initializer.GetManager<UIManager, IUIManager>();
    }

    protected OnboardingScreenUIController GetOnboardingScreenUIController(bool refresh) {
        OnboardingScreenUIController onboardingScreen = uiManager.LoadUI<OnboardingScreenUIController>(refresh);
        return onboardingScreen;
    }

}
