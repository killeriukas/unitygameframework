﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class BlockOnboardingEvent : BaseSequenceOnboardingEvent {

        [JsonProperty]
        private HashSet<string> completedEventIds;

        private IOnboardingEvent currentEvent;

        protected BlockOnboardingEvent() {
        }

        public BlockOnboardingEvent(string id) : base(id) {
            this.completedEventIds = new HashSet<string>();
        }

        protected override void StartOnboardingEventInternal() {
            currentEvent = currentEventQueue.Peek();
            currentEvent.Setup(initializer, this);
            currentEvent.StartOnboardingEvent();
        }

        public override void FinishOnboardingEvent(IOnboardingEvent finishedEvent) {
            string finishedEventId = finishedEvent.id;
            finishedEvent.Dispose();

            completedEventIds.Add(finishedEventId);
            IOnboardingEvent oldOnboardingEvent = currentEventQueue.Dequeue();

            if(currentEventQueue.Count > 0) {
                ContinueOnboardingEvent(true, finishedEventId);

                currentEvent = currentEventQueue.Peek();
                currentEvent.Setup(initializer, this);
                currentEvent.StartOnboardingEvent();
            } else {
                FinishOnboardingEvent();
            }
        }

        public override void Upgrade(IOnboardingEvent oldOnboardingEvent) {
            BlockOnboardingEvent oldOnboardingEventCast = (BlockOnboardingEvent)oldOnboardingEvent;

            foreach(string completedEventId in oldOnboardingEventCast.completedEventIds) {
                completedEventIds.Add(completedEventId);
                RemoveEventForUpgrade(completedEventId);
            }

            //do this later, so that the events which are completed, should not be upgraded - saves time!
            base.Upgrade(oldOnboardingEvent);
        }

    }


}