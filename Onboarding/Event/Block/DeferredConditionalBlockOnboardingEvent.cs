﻿using Newtonsoft.Json;
using TMI.Core;
using TMI.LogManagement;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class DeferredConditionalBlockOnboardingEvent : DeferredBlockOnboardingEvent {

        [JsonProperty]
        private ISequenceCondition condition;

        [JsonProperty]
        private bool wasConditionTested = false;

        [JsonProperty]
        private bool canFinishSequence = false;

        private DeferredConditionalBlockOnboardingEvent() {
        }

        public DeferredConditionalBlockOnboardingEvent(IOnboardingEvent waitForOnboardingEvent, ISequenceCondition condition, string id) : base(waitForOnboardingEvent, id) {
            this.condition = condition;
        }

        public override void Setup(IInitializer initializer, ISequenceOnboarding onboarding) {
            base.Setup(initializer, onboarding);
            condition.Setup(initializer);
        }

        public override void FinishOnboardingEvent(IOnboardingEvent finishedEvent) {
            Logging.Log(this, "Finishing conditional block onboarding event with wasConditionTested=[{0}].", wasConditionTested);
            if(!wasConditionTested) {
                wasConditionTested = true;
                canFinishSequence = condition.CanFinishSequence();
            }

            Logging.Log(this, "Conditional block onboarding event canFinishSequence=[{0}].", canFinishSequence);
            if(canFinishSequence) {
                FinishOnboardingEvent();
            } else {
                base.FinishOnboardingEvent(finishedEvent);
            }

        }

        public override void Upgrade(IOnboardingEvent oldOnboardingEvent) {
            base.Upgrade(oldOnboardingEvent);
            DeferredConditionalBlockOnboardingEvent oldOnboardingEventCast = (DeferredConditionalBlockOnboardingEvent)oldOnboardingEvent;
            wasConditionTested = oldOnboardingEventCast.wasConditionTested;
            canFinishSequence = oldOnboardingEventCast.canFinishSequence;
        }

    }


}