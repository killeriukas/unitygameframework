﻿using Newtonsoft.Json;
using TMI.Core;
using TMI.LogManagement;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class ConditionalBlockOnboardingEvent : BlockOnboardingEvent {

        [JsonProperty]
        private ISequenceCondition condition;

        [JsonProperty]
        private bool wasConditionTested = false;

        [JsonProperty]
        private bool canFinishSequence = false;

        private ConditionalBlockOnboardingEvent() {
        }

        public ConditionalBlockOnboardingEvent(ISequenceCondition condition, string id) : base(id) {
            this.condition = condition;
        }

        protected override void StartOnboardingEventInternal() {
            Logging.Log(this, "Starting conditional block onboarding event with wasConditionTested=[{0}].", wasConditionTested);
            if(!wasConditionTested) {
                wasConditionTested = true;
                canFinishSequence = condition.CanFinishSequence();
            }

            Logging.Log(this, "Conditional block onboarding event canFinishSequence=[{0}].", canFinishSequence);
            if(canFinishSequence) {
                FinishOnboardingEvent();
            } else {
                base.StartOnboardingEventInternal();
            }
        }

        public override void Setup(IInitializer initializer, ISequenceOnboarding onboarding) {
            base.Setup(initializer, onboarding);
            condition.Setup(initializer);
        }

        public override void Upgrade(IOnboardingEvent oldOnboardingEvent) {
            base.Upgrade(oldOnboardingEvent);
            ConditionalBlockOnboardingEvent oldOnboardingEventCast = (ConditionalBlockOnboardingEvent)oldOnboardingEvent;

            wasConditionTested = oldOnboardingEventCast.wasConditionTested;
            canFinishSequence = oldOnboardingEventCast.canFinishSequence;
        }

    }


}