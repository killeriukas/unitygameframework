﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TMI.OnboardingManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class DeferredBlockOnboardingEvent : BaseSequenceOnboardingEvent {

        [JsonProperty]
        private HashSet<string> completedEventIds;

        [JsonProperty]
        private IOnboardingEvent waitForOnboardingEvent;

        private IOnboardingEvent currentEvent;

        protected DeferredBlockOnboardingEvent() {
        }

        public DeferredBlockOnboardingEvent(IOnboardingEvent waitForOnboardingEvent, string id) : base(id) {
            this.waitForOnboardingEvent = waitForOnboardingEvent;
            this.completedEventIds = new HashSet<string>();
        }

        protected override void StartOnboardingEventInternal() {
            waitForOnboardingEvent.Setup(initializer, this);
            waitForOnboardingEvent.StartOnboardingEvent();
        }

        public override void FinishOnboardingEvent(IOnboardingEvent finishedEvent) {
            string finishedEventId = finishedEvent.id;
            finishedEvent.Dispose();

            //dispose of the old pending event, if it wasn't a condition
            if(currentEvent != null) {
                completedEventIds.Add(finishedEventId);
                IOnboardingEvent oldOnboardingEvent = currentEventQueue.Dequeue();

                //if count is less - the finish onboarding event will be called
                //no need to duplicate the calls
                if(currentEventQueue.Count > 0) {
                    ContinueOnboardingEvent(true, finishedEventId);
                }
            }

            if(currentEventQueue.Count > 0) {
                currentEvent = currentEventQueue.Peek();
                currentEvent.Setup(initializer, this);
                currentEvent.StartOnboardingEvent();
            } else {
                FinishOnboardingEvent();
            }
        }

        public override void Upgrade(IOnboardingEvent oldOnboardingEvent) {
            DeferredBlockOnboardingEvent oldOnboardingEventCast = (DeferredBlockOnboardingEvent)oldOnboardingEvent;

            foreach(string completedEventId in oldOnboardingEventCast.completedEventIds) {
                completedEventIds.Add(completedEventId);
                RemoveEventForUpgrade(completedEventId);
            }

            //do this later, so that the events which are completed, should not be upgraded - saves time!
            base.Upgrade(oldOnboardingEvent);
        }


    }
}