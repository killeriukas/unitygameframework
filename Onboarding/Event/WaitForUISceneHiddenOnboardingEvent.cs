﻿using Newtonsoft.Json;
using TMI.OnboardingManagement;
using TMI.UI;

public class WaitForUISceneHiddenOnboardingEvent : BaseOnboardingEvent {

    [JsonProperty]
    private string sceneName;

    protected WaitForUISceneHiddenOnboardingEvent() {
    }

    public WaitForUISceneHiddenOnboardingEvent(string sceneName, string id) : base(id) {
        this.sceneName = sceneName;
    }

    protected override void StartOnboardingEventInternal() {
        Listen<UISceneHiddenNotification>(this, OnUISceneHidden);
    }

    private void OnUISceneHidden(UISceneHiddenNotification notification) {
        if(notification.IsMyNotification(sceneName)) {
            FinishOnboardingEvent();
        }
    }

}
