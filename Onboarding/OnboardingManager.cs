﻿using Assets.USecurity;
using System.Collections.Generic;
using TMI.Core;
using TMI.Helper;
using TMI.LogManagement;
using TMI.UI;
using UnityEngine;

namespace TMI.OnboardingManagement {

    public abstract class OnboardingManager : BaseNotificationManager, IOnboardingManager, IOnboardingEventFinisher {

        private Dictionary<string, IUIOnboardingItemLocator> onboardingLocatorById = new Dictionary<string, IUIOnboardingItemLocator>();

        public static bool isOnboardingEnabled = true;
        private const bool encode = true;

        private const string mangler = "61q6UiXRmJVBpE&*DeD#jFRnH#2D3HmVXkdzg0MAFma368Jpo*^zxugBD6Pa6e@J";
        private const string salt = "@B#OC!0z763mHW5sBsRx0PwjZIVmmjN4OB794uB3Vxq%MfpwusUga&Fa31l4B7NQ";
        private const string vi = "o!CVPFkN@9KhJ14r";

        private Onboarding onboarding;

        public override void Setup(IInitializer initializer, bool isNew) {
            base.Setup(initializer, isNew);

            if(isOnboardingEnabled) {
                if(isNew) {
                    Version newestVersion = new Version(Application.version);

                    bool hasOnboardingStarted = Onboarding.Exists();
                    if(hasOnboardingStarted) {
                        onboarding = LoadOnboarding();

                        if(onboarding.IsUpgradeRequired(newestVersion)) {
                            Logging.Log(this, "Upgrading onboarding version.");
                            Onboarding newOnboarding = new Onboarding(newestVersion);
                            PopulateOnboarding(newOnboarding);

                            newOnboarding.Upgrade(onboarding);
                            SaveOnboarding(newOnboarding);
                            onboarding = newOnboarding;
                            Logging.Log(this, "Onboarding upgrade finished.");
                        }

                        onboarding.Setup(initializer, this);
                    } else {
                        onboarding = new Onboarding(newestVersion);
                        onboarding.Setup(initializer, this);
                        PopulateOnboarding(onboarding);

                        SaveOnboarding(onboarding);
                    }
                }
            }
        }

        private static Onboarding LoadOnboarding() {
            string content = FileHelper.ReadWithBackup(FileConstant.onboardingFile);

            if(encode) {
                content = AES.Decrypt(content, mangler, salt, vi);
            }

            Onboarding onboarding = SerializationHelper.Deserialize<Onboarding>(content);
            return onboarding;
        }

        private static void SaveOnboarding(Onboarding _onboarding) {
            string content = SerializationHelper.Serialize(_onboarding);

            if(encode) {
                content = AES.Encrypt(content, mangler, salt, vi);
            }

            FileHelper.WriteWithBackup(FileConstant.onboardingFile, content);
        }

        protected abstract void PopulateOnboarding(Onboarding data);

        public void Initialize() {
            if(isOnboardingEnabled) {
                if(!onboarding.hasFinished) {
                    onboarding.StartOnboarding();
                }
            }
        }

        public void MarkCurrentOnboardingEventFinished() {
            Logging.Log(this, "Marking current onboarding event as finished.");
            SaveOnboarding(onboarding);
        }

        public IUIOnboardingItemLocator GetOnboardingItemLocator(string locatorId) {
            return onboardingLocatorById[locatorId];
        }

        public void RegisterLocator(IUIOnboardingItemLocator locator) {
            onboardingLocatorById.Add(locator.locatorId, locator);
        }

        public void UnregisterLocator(IUIOnboardingItemLocator locator) {
            onboardingLocatorById.Remove(locator.locatorId);
        }

    }

}