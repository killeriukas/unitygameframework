﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using TMI;
using TMI.Core;
using TMI.LogManagement;
using TMI.OnboardingManagement;
using TMI.Save;

[JsonObject(MemberSerialization.OptIn)]
public class Onboarding : BaseModelGeneric<Onboarding.Data>, ISequenceOnboarding, ILoggable {

    [Serializable]
    public class Data : IModel {

        [NewField(true, "x.x.x")]
        public TMI.Version version;

        public HashSet<string> completedEventIds = new HashSet<string>();
        public List<IOnboardingEvent> pendingEventsList = new List<IOnboardingEvent>();

    }

    private Queue<IOnboardingEvent> pendingEventsQueue;

    private IInitializer initializer;
    private IOnboardingEventFinisher onboardingEventFinisher;

    [OnSerializing]
    private void OnSerializing(StreamingContext context) {
        model.pendingEventsList = new List<IOnboardingEvent>(pendingEventsQueue);
    }

    [OnDeserialized]
    private void OnDeserialized(StreamingContext context) {
        pendingEventsQueue = new Queue<IOnboardingEvent>(model.pendingEventsList);
    }

    private Onboarding() {
    }

    public Onboarding(TMI.Version version) {
        model.version = version;
        pendingEventsQueue = new Queue<IOnboardingEvent>();
    }

    public void Setup(IInitializer initializer, IOnboardingEventFinisher onboardingEventFinisher) {
        this.initializer = initializer;
        this.onboardingEventFinisher = onboardingEventFinisher;
    }

    public bool IsUpgradeRequired(TMI.Version newestVersion) {
        return model.version < newestVersion;
    }

    public void EnqueueEvent(IOnboardingEvent onboardingEvent) {
        if(model.completedEventIds.Contains(onboardingEvent.id)) {
            Logging.LogWarning(this, "Skipping the onboarding event id [{0}], because it's completed.", onboardingEvent.id);
        } else {
            Logging.Log(this, "Enqueueing onboarding event [{0}].", onboardingEvent.id);
            pendingEventsQueue.Enqueue(onboardingEvent);
        }
    }

    public void StartOnboarding() {
        IOnboardingEvent currentEvent = pendingEventsQueue.Peek();
        currentEvent.Setup(initializer, this);

        Logging.Log(this, "Starting onboarding event [{0}].", currentEvent.id);
        currentEvent.StartOnboardingEvent();
    }

    public bool hasFinished {
        get {
            return pendingEventsQueue.Count == 0;
        }
    }

    public static bool Exists() {
        bool onboardingFileExists = File.Exists(FileConstant.onboardingFile);
        return onboardingFileExists;
    }

    public void Upgrade(Onboarding oldOnboarding) {
        model.pendingEventsList = new List<IOnboardingEvent>(pendingEventsQueue);

        //copy over all the finished event ids
        Data oldModel = oldOnboarding.model;
        foreach(string completedEventId in oldModel.completedEventIds) {
            model.completedEventIds.Add(completedEventId);
            model.pendingEventsList.RemoveAll(x => x.id.Equals(completedEventId));
        }

        //take old pending events and find the same event in a new list. Upgrade it with an old one
        foreach(IOnboardingEvent oldOnboardingEvent in oldModel.pendingEventsList) {
            IOnboardingEvent newSameOnboardingEvent = model.pendingEventsList.Find(x => x.id.Equals(oldOnboardingEvent.id));
            newSameOnboardingEvent.Upgrade(oldOnboardingEvent);
        }

        pendingEventsQueue = new Queue<IOnboardingEvent>(model.pendingEventsList);
    }

    public void FinishOnboardingEvent(IOnboardingEvent finishedEvent) {
        string finishedEventId = finishedEvent.id;
        Logging.Log(this, "Finishing onboarding event [{0}].", finishedEventId);

        //dispose of the old pending event, which has to be the finished event
        IOnboardingEvent oldOnboardingEvent = pendingEventsQueue.Dequeue();

        Assert.IsFalse<ArgumentException>(finishedEventId.Equals(oldOnboardingEvent.id),
            "Cannot finish event with an id different to the current event. Finished Id: " + finishedEventId + " Current Id: " + oldOnboardingEvent.id);

        finishedEvent.Dispose();
        model.completedEventIds.Add(finishedEventId);

        //this saves the onboarding data onto the disk
        onboardingEventFinisher.MarkCurrentOnboardingEventFinished();

        if(pendingEventsQueue.Count > 0) {
            StartOnboarding();
        }
    }

    public void ContinueOnboardingEvent(bool save, string eventId) {
        Logging.Log(this, "Continue onboarding event [{0}] and save=[{1}].", eventId, save);
        if(save) {
            onboardingEventFinisher.MarkCurrentOnboardingEventFinished();
        }
    }

}
