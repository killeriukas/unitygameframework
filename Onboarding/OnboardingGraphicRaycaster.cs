﻿using System.Collections.Generic;
using TMI.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TMI.OnboardingManagement {

    public class OnboardingGraphicRaycaster : GraphicRaycaster {

        [SerializeField]
        private GameObject fullBackgroundGameObject;

        [SerializeField]
        private GameObject maskGameObject;

        private HashSet<GameObject> dismissRaycastGameObjectHashSet = new HashSet<GameObject>();

        protected override void Awake() {
            base.Awake();
            dismissRaycastGameObjectHashSet.Add(fullBackgroundGameObject);
            dismissRaycastGameObjectHashSet.Add(maskGameObject);
        }

        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList) {
            base.Raycast(eventData, resultAppendList);

            bool hasHitRaycastMask = false;
            foreach(RaycastResult raycast in resultAppendList) {
                if(maskGameObject == raycast.gameObject) {
                    hasHitRaycastMask = true;
                    break;
                }
            }

            //if person didn't click on the mask object, just clear the mask and leave the rest
            //we want the background to catch the clicks
            if(hasHitRaycastMask) {
                resultAppendList.RemoveAll(x => dismissRaycastGameObjectHashSet.Contains(x.gameObject));

                //remove everything, just leave the object with UIOnboardingItemLocator component on it
                //this is just to safeguard against the lock due to another screen popping up between the target area and onboarding screen
                resultAppendList.RemoveAll(x => x.gameObject.GetComponent<UIOnboardingItemLocator>() == null);
            } else {
                resultAppendList.RemoveAll(x => x.gameObject == maskGameObject);
            }

        }

    }


}