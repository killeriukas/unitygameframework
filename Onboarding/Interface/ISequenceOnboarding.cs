﻿using TMI.OnboardingManagement;

public interface ISequenceOnboarding {
    void ContinueOnboardingEvent(bool save, string eventId);
    void FinishOnboardingEvent(IOnboardingEvent finishedEvent);
}
