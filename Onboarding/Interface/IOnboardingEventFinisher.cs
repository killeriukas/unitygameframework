﻿
namespace TMI.OnboardingManagement {

    public interface IOnboardingEventFinisher {
        void MarkCurrentOnboardingEventFinished();
    }

}