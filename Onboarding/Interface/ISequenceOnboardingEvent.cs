﻿using System;
using TMI.Core;
using TMI.LogManagement;

namespace TMI.OnboardingManagement {

    public interface ISequenceOnboardingEvent : IDisposable, ILoggable {
        string id { get; }
        void StartOnboardingEvent();
        void ContinueOnboardingEvent(bool save);
        void Setup(IInitializer initializer, IOnboardingEventFinisher onboardingEventFinisher);
    }

}