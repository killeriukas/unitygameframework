﻿using TMI.Core;
using TMI.UI;

namespace TMI.OnboardingManagement {

    public interface IOnboardingManager : IManager {
        void Initialize();
        void RegisterLocator(IUIOnboardingItemLocator locator);
        void UnregisterLocator(IUIOnboardingItemLocator locator);
        IUIOnboardingItemLocator GetOnboardingItemLocator(string id);
    }

}