﻿using System;
using TMI.Core;
using TMI.LogManagement;

namespace TMI.OnboardingManagement {

    public interface IOnboardingEvent : IDisposable, ILoggable {
        string id { get; }
        void StartOnboardingEvent();
        void Setup(IInitializer initializer, ISequenceOnboarding onboarding);
        void Upgrade(IOnboardingEvent oldOnboardingEvent);
    }

}