﻿using System;
using TMI.Helper;
using TMI.LogManagement;
using TMI.TimeManagement;

#if USE_FACEBOOK_ANALYTICS
using Facebook.Unity;

namespace TMI.AnalyticsManagement {

    public class FacebookAnalyticsTracker : IAnalyticsTracker, IAnalyticsTrackerExecutor, ILoggable {

        public event Action onCompleted;
        public event Action onCanceled;
        public event Action onFailed;

        private FacebookAnalyticsTracker() {
        }

        private void OnFacebookInitializationCompleted() {
            if(FB.IsInitialized) {
                Logging.Log(this, "Facebook initialization successful!");
                FB.ActivateApp();
                GeneralHelper.CallNullCheck(onCompleted);
            } else {
                Logging.LogWarning(this, "Facebook initialization failed!");
                GeneralHelper.CallNullCheck(onFailed);
            }
        }

        public static IAnalyticsTracker Create() {
            return new FacebookAnalyticsTracker();
        }

        public void Register() {
            if(FB.IsInitialized) {
                Logging.Log(this, "Facebook has been initialized before the on Register() call.");
                FB.ActivateApp();
                GeneralHelper.CallNullCheck(onCompleted);
            } else {
                Logging.Log(this, "Started initializing Facebook SDK.");
                FB.Init(OnFacebookInitializationCompleted);
            }
        }

        public void LogEvent(IAnalyticsTrackerEvent trackerEvent) {
            FB.LogAppEvent(trackerEvent.id, null, trackerEvent.data);
        }

        public void Dispose() {
            
        }

        public void Pause(MonotonicTime time, bool hasQuitApplication) {
            
        }

        public void Unpause(MonotonicTime time) {
            if(FB.IsInitialized) {
                Logging.Log(this, "Facebook is already initialized!");
                FB.ActivateApp();
                GeneralHelper.CallNullCheck(onCompleted);
            } else {
                Logging.Log(this, "Facebook is not initialized! Starting to initialize again.");
                FB.Init(OnFacebookInitializationCompleted);
            }
        }

        public IAnalyticsCustomTracker CreateCustomEvent<TCustomEventType>() where TCustomEventType : IAnalyticsCustomTracker {
            throw new InvalidOperationException("CreateCustomEvent() method is not valid for FacebookAnalytics class.");
        }
    }
}

#endif