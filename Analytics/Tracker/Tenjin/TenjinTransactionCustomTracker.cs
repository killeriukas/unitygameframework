﻿using TMI.LogManagement;

#if USE_TENJIN_ANALYTICS

namespace TMI.AnalyticsManagement {

    public class TenjinTransactionCustomTracker : IAnalyticsCustomTracker, ILoggable {

        private readonly BaseTenjin tenjin;

        public TenjinTransactionCustomTracker(BaseTenjin tenjin) {
            this.tenjin = tenjin;
        }

        public void LogCustomEvent(IAnalyticsTrackerEvent trackerEvent) {
            TenjinIapProductPurchaseAdapter adapter = (TenjinIapProductPurchaseAdapter)trackerEvent.data["data"];

            Logging.Log(this, "Logging Tenjin transaction: " +
                "ProductId [{0}], " +
                "CurrencyCode [{1}], " +
                "Quantity [{2}], " +
                "UnitPrice [{3}], " +
                "TransactionId [{4}], " +
                "Receipt [{5}], " +
                "Signature [{6}]",
                adapter.productId,
                adapter.currencyCode,
                adapter.quantity,
                adapter.unitPrice,
                adapter.transactionId,
                adapter.receipt,
                adapter.signature);

            tenjin.Transaction(adapter.productId,
                adapter.currencyCode,
                adapter.quantity,
                (double)adapter.unitPrice,
                adapter.transactionId,
                adapter.receipt,
                adapter.signature);
        }
    }
}

#endif