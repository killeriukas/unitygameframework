﻿using System;
using TMI.Helper;
using TMI.LogManagement;
using TMI.TimeManagement;

#if USE_TENJIN_ANALYTICS

namespace TMI.AnalyticsManagement {

    public class TenjinAnalyticsTracker : IAnalyticsTracker, IAnalyticsTrackerExecutor, ILoggable {

        public event Action onCompleted;
        public event Action onCanceled;
        public event Action onFailed;

        private readonly BaseTenjin tenjin;

        private TenjinAnalyticsTracker(string apiKey) {
            tenjin = Tenjin.getInstance(apiKey);
        }

        public static IAnalyticsTracker Create(string apiKey) {
            return new TenjinAnalyticsTracker(apiKey);
        }

        public void Register() {
            Logging.Log(this, "Tenjin is connecting within Register() call.");
            tenjin.Connect();
        }

        public void LogEvent(IAnalyticsTrackerEvent trackerEvent) {
            //TODO: we do not log normal events on tenjin just yet
            //tenjin.Transaction();
        }

        public IAnalyticsCustomTracker CreateCustomEvent<TCustomEventType>() where TCustomEventType : IAnalyticsCustomTracker {
            TCustomEventType customEvent = GeneralHelper.CreateInstance<TCustomEventType>(tenjin);
            return customEvent;
        }

        public void Dispose() {
            
        }

        public void Pause(MonotonicTime time, bool hasQuitApplication) {
            
        }

        public void Unpause(MonotonicTime time) {
            Logging.Log(this, "Tenjin is connecting within Unpause() call.");
            tenjin.Connect();
        }
    }
}

#endif