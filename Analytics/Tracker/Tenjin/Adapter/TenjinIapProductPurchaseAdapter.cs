﻿using TMI.InAppPurchase;

namespace TMI.AnalyticsManagement {

    public class TenjinIapProductPurchaseAdapter {

        public string productId { get; private set; }
        public string currencyCode { get; private set; }
        public int quantity { get; private set; }
        public decimal unitPrice { get; private set; }
        public string transactionId { get; private set; }
        public string receipt { get; private set; }
        public string signature { get; private set; }

        public TenjinIapProductPurchaseAdapter(IIapPurchaseResult result) {

            IIapProduct product = result.product;
            productId = product.id;
            currencyCode = product.currencyCode;
            quantity = 1;
            unitPrice = product.actualPrice;

            IIapTransaction transaction = result.transaction;

#if UNITY_ANDROID
            transactionId = null;
#elif UNITY_IOS
            transactionId = transaction.id;
#endif

            receipt = transaction.receipt;
            signature = transaction.signature;
        }

    }

}