﻿using System;
using System.Collections.Generic;
using TMI.LogManagement;
using TMI.TimeManagement;

#if USE_FIREBASE_ANALYTICS
using Firebase.Analytics;

namespace TMI.AnalyticsManagement {

    public class FirebaseAnalyticsTracker : IAnalyticsTracker, IAnalyticsTrackerExecutor, ILoggable {

        public event Action onCompleted;
        public event Action onCanceled;
        public event Action onFailed;

        private FirebaseAnalyticsTracker() {
        }

        public static IAnalyticsTracker Create() {
            return new FirebaseAnalyticsTracker();
        }

        public void Register() {
        }

        public void LogEvent(IAnalyticsTrackerEvent trackerEvent) {
            List<Parameter> logParameters = new List<Parameter>();
            foreach(var kvp in trackerEvent.data) {
                Parameter newParameter = new Parameter(kvp.Key, kvp.Value.ToString());
                logParameters.Add(newParameter);
            }

            FirebaseAnalytics.LogEvent(trackerEvent.id, logParameters.ToArray());
        }

        public void Dispose() {
            
        }

        public void Pause(MonotonicTime time, bool hasQuitApplication) {
        }

        public void Unpause(MonotonicTime time) {
        }

        public IAnalyticsCustomTracker CreateCustomEvent<TCustomEventType>() where TCustomEventType : IAnalyticsCustomTracker {
            throw new InvalidOperationException("CreateCustomEvent() method is not valid for FirebaseAnalytics class.");
        }
    }


}

#endif