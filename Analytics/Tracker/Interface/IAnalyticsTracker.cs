﻿using System;

namespace TMI.AnalyticsManagement {

    public interface IAnalyticsTracker {
        event Action onCompleted;
        event Action onCanceled;
        event Action onFailed;
    }

}