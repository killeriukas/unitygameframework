﻿using System;
using TMI.TimeManagement;

namespace TMI.AnalyticsManagement {

    public interface IAnalyticsTrackerExecutor : IDisposable {
        void Register();
        void Pause(MonotonicTime time, bool hasQuitApplication);
        void Unpause(MonotonicTime time);
        void LogEvent(IAnalyticsTrackerEvent trackerEvent);
        IAnalyticsCustomTracker CreateCustomEvent<TCustomEventType>() where TCustomEventType : IAnalyticsCustomTracker;
    }

}