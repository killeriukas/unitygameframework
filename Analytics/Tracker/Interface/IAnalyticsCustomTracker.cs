﻿
namespace TMI.AnalyticsManagement {

    public interface IAnalyticsCustomTracker {
        void LogCustomEvent(IAnalyticsTrackerEvent trackerEvent);
    }

}