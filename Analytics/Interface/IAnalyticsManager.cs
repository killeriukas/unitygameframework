﻿using TMI.Core;

namespace TMI.AnalyticsManagement {

    public interface IAnalyticsManager : IManager {
        void Register(IAnalyticsTracker tracker);
        void LogEvent(IAnalyticsTrackerEvent trackerEvent);
        void LogCustomEvent<TAnalyticsTrackerType, TCustomEventType>(IAnalyticsTrackerEvent trackerEvent)
            where TAnalyticsTrackerType : IAnalyticsTracker, IAnalyticsTrackerExecutor
            where TCustomEventType : IAnalyticsCustomTracker;
    }


}