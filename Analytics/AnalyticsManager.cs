﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.LogManagement;

namespace TMI.AnalyticsManagement {

    public abstract class AnalyticsManager : BaseNotificationManager, IAnalyticsManager {

        private readonly Dictionary<Type, IAnalyticsTrackerExecutor> analyticsTrackerExecutors = new Dictionary<Type, IAnalyticsTrackerExecutor>();

        public override void Setup(IInitializer initializer, bool isNew) {
            base.Setup(initializer, isNew);

            if(isNew) {
                Listen<GamePausedNotification>(this, OnApplicationPaused);
                Listen<GameUnpausedNotification>(this, OnApplicationUnpaused);
            }
        }

        private void OnApplicationUnpaused(GameUnpausedNotification notification) {
            foreach(var kvp in analyticsTrackerExecutors) {
                kvp.Value.Unpause(notification.value);
            }
        }

        private void OnApplicationPaused(GamePausedNotification notification) {
            foreach(var kvp in analyticsTrackerExecutors) {
                kvp.Value.Pause(notification.value, notification.hasQuitApplication);
            }
        }

        public void Register(IAnalyticsTracker tracker) {
            Assert.IsNull(tracker, "Passed analytics tracker cannot be null!");

            //we know that IAnalyticsTracker is actually an executor too
            IAnalyticsTrackerExecutor analyticsExecutor = (IAnalyticsTrackerExecutor)tracker;

            Type analyticssExecutorType = analyticsExecutor.GetType();
            analyticsTrackerExecutors.Add(analyticssExecutorType, analyticsExecutor);
            analyticsExecutor.Register();
        }

        public void LogEvent(IAnalyticsTrackerEvent trackerEvent) {
            List<string> logData = new List<string>();
            foreach(var kvp in trackerEvent.data) {
                string logLine = kvp.Key + ":" + kvp.Value.ToString();
                logData.Add(logLine);
            }
            string logDataString = string.Join(", ", logData.ToArray());
            Logging.Log(this, "Logging an event of [{0}]. Data: {1}.", trackerEvent.id, logDataString);

            IEnumerable<Type> specificTrackers = trackerEvent.specificTrackersOnly;
            if(specificTrackers == null) {
                foreach(var kvp in analyticsTrackerExecutors) {
                    kvp.Value.LogEvent(trackerEvent);
                }
            } else {
                foreach(Type trackerType in specificTrackers) {
                    IAnalyticsTrackerExecutor analyticsTracker = analyticsTrackerExecutors[trackerType];
                    analyticsTracker.LogEvent(trackerEvent);
                }
            }
        }

        public void LogCustomEvent<TAnalyticsTrackerType, TCustomEventType>(IAnalyticsTrackerEvent trackerEvent)
            where TAnalyticsTrackerType : IAnalyticsTracker, IAnalyticsTrackerExecutor
            where TCustomEventType : IAnalyticsCustomTracker {

            Type customEventType = typeof(TCustomEventType);
            Type trackerType = typeof(TAnalyticsTrackerType);
            Logging.Log(this, "Logging a custom event [{0}] for the tracker [{1}] with id [{2}].", customEventType.Name, trackerType.Name, trackerEvent.id);

            IAnalyticsTrackerExecutor selectedTracker = analyticsTrackerExecutors[trackerType];
            IAnalyticsCustomTracker customAnalyticsTracker = selectedTracker.CreateCustomEvent<TCustomEventType>();
            customAnalyticsTracker.LogCustomEvent(trackerEvent);
        }

        protected override void OnDestroy() {
            foreach(var kvp in analyticsTrackerExecutors) {
                kvp.Value.Dispose();
            }
            analyticsTrackerExecutors.Clear();
            base.OnDestroy();
        }

    }

}