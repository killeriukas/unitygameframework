﻿using System;
using System.Collections.Generic;

namespace TMI.AnalyticsManagement {

    public class GenericAnalyticsTrackerEvent : IAnalyticsTrackerEvent {

        public string id { get; set; }
        public Dictionary<string, object> data { get; private set; }
        public IEnumerable<Type> specificTrackersOnly { get { return null; } } //null means include all

        public GenericAnalyticsTrackerEvent() {
            data = new Dictionary<string, object>();
        }
    }

}