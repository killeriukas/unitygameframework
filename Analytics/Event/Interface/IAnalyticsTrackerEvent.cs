﻿using System;
using System.Collections.Generic;

namespace TMI.AnalyticsManagement {

    public interface IAnalyticsTrackerEvent {
        string id { get; }
        Dictionary<string, object> data { get; }
        IEnumerable<Type> specificTrackersOnly { get; }
    }

}