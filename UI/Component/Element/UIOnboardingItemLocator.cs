﻿using TMI.UI.Extension;
using TMI.OnboardingManagement;
using TMI.Core;
using UnityEngine;

namespace TMI.UI {

    [RequireComponent(typeof(OnboardingItemLocator))]
    public class UIOnboardingItemLocator : UIComponentWithId<OnboardingItemLocator>, IUIOnboardingItemLocator {

        [SerializeField]
        private string onboardingLocatorId;

        private IOnboardingManager onboardingManager;

        private bool isInitialized = false;

        public Vector2 size { get { return component.size; } }
        public Vector2 itemPosition { get { return component.itemPosition; } }
        public Vector2 actualReferenceResolution { get { return component.actualReferenceResolution; } }

        public string locatorId {
            get {
                return onboardingLocatorId;
            }
            set {
                onboardingLocatorId = value;

                if(!isInitialized) {
                    isInitialized = true;
                    onboardingManager.RegisterLocator(this);
                }
            }
        }

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            onboardingManager = initializer.GetManager<IOnboardingManager>();

            if(!string.IsNullOrEmpty(onboardingLocatorId)) {
                isInitialized = true;
                onboardingManager.RegisterLocator(this);
            }
        }

        protected override void OnDestroy() {
            onboardingManager?.UnregisterLocator(this);
            isInitialized = false;
            base.OnDestroy();
        }

    }

}