﻿using UnityEngine;

namespace TMI.UI {

    public interface IUIOnboardingItemLocator {
        string locatorId { get; }
        Vector2 size { get; }
        Vector2 itemPosition { get; }
        Vector2 actualReferenceResolution { get; }
    }

}