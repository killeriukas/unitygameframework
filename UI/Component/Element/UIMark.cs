﻿using TMI.UI.Extension;
using UnityEngine;

namespace TMI.UI {

    [RequireComponent(typeof(Mark))]
    public class UIMark : UIComponentWithId<Mark> {

        public void AddMark(string id) {
            component.AddMark(id);
        }

    }
}