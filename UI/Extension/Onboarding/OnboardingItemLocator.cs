﻿using UnityEngine;

namespace TMI.UI.Extension {

    public class OnboardingItemLocator : UIComponent<RectTransform> {

        private RectTransform mainCanvasRectTransform;

        protected override void OnInitialize() {
            base.OnInitialize();
            Canvas canvas = GetComponentInParent<Canvas>();
            canvas = canvas.rootCanvas;

            mainCanvasRectTransform = canvas.GetComponent<RectTransform>();
        }

        public Vector2 actualReferenceResolution {
            get {
                return mainCanvasRectTransform.rect.size;
            }
        }

        public Vector2 size {
            get {
                Rect rect = component.rect;
                return rect.size;
            }
        }

        public Vector2 itemPosition {
            get {
                Vector3 worldPos = component.TransformPoint(Vector2.zero);
                return worldPos;
            }
        }

    }

}