﻿using TMI.Core;
using UnityEngine;

namespace TMI.UI.Extension {

    [RequireComponent(typeof(UIToggle))]
    public class AnalyticsToggleValueChangeTracker : BaseAnalyticsClickTracker<UIToggle> {

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            component.onValueChanged += OnValueChanged;
        }

        private void OnValueChanged(bool currentValue) {
            //we care only when the player presses on the tab, and not when it's closed by code
            if(currentValue) {
                LogClick();
            }
        }

        protected override void OnDestroy() {
            component.onValueChanged -= OnValueChanged;
            base.OnDestroy();
        }
    }

}