﻿using TMI.Core;
using TMI.Helper;
using TMI.LogManagement;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI.Extension {

    [RequireComponent(typeof(UIButton))]
    public class AnalyticsButtonClickTracker : BaseAnalyticsClickTracker<UIButton> {

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            Logging.Log(this, "Setting up the AnalyticsButtonClickTracker in [{0}] on [{1}].", sceneName, GeneralHelper.GenerateTransformHierarchyString(transform));
            component.onButtonClick += OnButtonClicked;
        }

        private void OnButtonClicked(PointerEventData data) {
            LogClick();
        }

        protected override void OnDestroy() {
            component.onButtonClick -= OnButtonClicked;
            base.OnDestroy();
        }
    }

}