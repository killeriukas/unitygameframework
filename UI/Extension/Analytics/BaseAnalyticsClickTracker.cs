﻿using System;
using TMI.AnalyticsManagement;
using TMI.Core;
using TMI.Helper;
using UnityEngine;

namespace TMI.UI.Extension {

    public abstract class BaseAnalyticsClickTracker<TComponent> : UIComponent {

        [SerializeField]
        private string clickName;

        protected TComponent component;

        private IAnalyticsManager analyticsManager;

        protected override void OnInitialize() {
            base.OnInitialize();
            Assert.IsTrue<ArgumentException>(string.IsNullOrEmpty(clickName), "Click name cannot be null on object: " + name);
            component = GetComponent<TComponent>();
        }

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            analyticsManager = initializer.GetManager<GameAnalyticsManager, IAnalyticsManager>();
        }

        protected void LogClick() {
            GenericAnalyticsTrackerEvent genericAnalyticsTrackerEvent = AnalyticsHelper.CreateButtonClickEvent(sceneName, clickName);
            analyticsManager.LogEvent(genericAnalyticsTrackerEvent);
        }

    }

}