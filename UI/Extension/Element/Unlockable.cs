﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.Notification;
using TMI.UnlockManagement;
using UnityEngine;

namespace TMI.UI.Extension {

    public class Unlockable : UIComponent, INotificationListener {

        private enum State {
            Locked = 0,
            Unlocked = 1
        }

        [SerializeField]
        private string unlockId;

        private State currentState = State.Locked;

        private UnlockManager unlockManager;
        private INotificationManager notificationManager;

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);

            string hierarchyName = GeneralHelper.GenerateTransformHierarchyString(transform);
            Assert.IsTrue<ArgumentNullException>(string.IsNullOrEmpty(unlockId), "Unlock id is null or empty. Please enter the valid id. Item: " + hierarchyName);

            notificationManager = initializer.GetManager<NotificationManager, INotificationManager>();
            unlockManager = initializer.GetManager<UnlockManager>();

            bool isUnlocked = unlockManager.IsUnlocked(unlockId);
            SetUnlock(isUnlocked);

            //be very careful of starting listening in the Setup() method;
            //if this component is being cloned, that means the main cell will get deactivated and then new cells will get cloned
            //however, the listener will still listen in the deactivated cell!!!
            notificationManager.Listen<ItemUnlockedNotification>(this, OnItemUnlocked, IsLocked);
        }

        private bool IsLocked(ItemUnlockedNotification notification) {
            return currentState == State.Locked;
        }

        private void OnItemUnlocked(ItemUnlockedNotification notification) {
            bool isUnlocked = unlockManager.IsUnlocked(unlockId);
            SetUnlock(isUnlocked);
        }

        private void SetUnlock(bool isUnlocked) {
            currentState = isUnlocked ? State.Unlocked : State.Locked;
            gameObject.SetActive(isUnlocked);
        }

        protected override void OnDestroy() {
            notificationManager.StopListenAll(this);
            base.OnDestroy();
        }
    }

}