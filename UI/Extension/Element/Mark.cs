﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.MarkManagement;
using TMI.Notification;
using UnityEngine;

namespace TMI.UI.Extension {

    public class Mark : UIComponent, INotificationListener {

        [SerializeField]
        private List<string> markIds;

        private MarkManager markManager;
        private INotificationManager notificationManager;

        public void AddMark(string id) {
            Assert.IsTrue<ArgumentException>(string.IsNullOrEmpty(id), "Mark id cannot be null or empty!");

            markIds.Add(id);

            bool _isMarked = isMarked;
            SetMark(_isMarked);
        }

        private bool isMarked {
            get {
                bool marked = false;
                foreach(string markId in markIds) {
                    if(markManager.IsMarked(markId)) {
                        marked = true;
                        break;
                    }
                }
                return marked;
            }
        }

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            notificationManager = initializer.GetManager<NotificationManager, INotificationManager>();
            markManager = initializer.GetManager<MarkManager>();

            bool _isMarked = isMarked;
            SetMark(_isMarked);

            //I think Setup() is not called on deactivated cells (anymore), and they are initialized when cells are cloned only

            //be very careful of starting listening in the Setup() method;
            //if this component is being cloned, that means the main cell will get deactivated and then new cells will get cloned
            //however, the listener will still listen in the deactivated cell!!!


            //HACK - add OnShutdown later on, and fix this!!!!!!!!!!!!!!!!!!!!!!!!
          //  notificationManager.Listen<MarkUnmarkedNotification>(this, OnMarkUnmarked);
        }

        private void OnMarkUnmarked(MarkUnmarkedNotification notification) {
            bool _isMarked = isMarked;
            SetMark(_isMarked);
        }

        private void SetMark(bool p_isMarked) {
            //HACK - add OnShutdown later on, and fix this!!!!!!!!!!!!!!!!!!!!!!!!
            if(p_isMarked && markIds.Count == 1) {
          //      Debug.LogError("adding Mark on: " + Helper.GeneralHelper.GenerateTransformHierarchyString(transform));
                notificationManager.Listen<MarkUnmarkedNotification>(this, OnMarkUnmarked);
            }

            gameObject.SetActive(p_isMarked);
        }

        protected override void OnDestroy() {

            //THIS ONE NEEDS TO BE IN A SHUTDOWN
            //OnDestroy is not called on inactive objects
           // Debug.LogError("destorying Mark on: " + Helper.GeneralHelper.GenerateTransformHierarchyString(transform));
            notificationManager?.StopListenAll(this);
            base.OnDestroy();
        }

  
    }

}