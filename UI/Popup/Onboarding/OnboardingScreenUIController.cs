﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI {

    public class OnboardingScreenUIController : BaseUIController {

        public enum ArrowPosition {
            Top = 0,
            Bottom,
            Left,
            Right
        }

        public enum DialogBoxPosition {
            Top = 0,
            Bottom
        }

        [SerializeField]
        private OnboardingHeroBehaviour onboardingHeroBehaviour;

        [SerializeField]
        private Transform mainDialogTransform;

        [SerializeField]
        private Transform bottomPositionTransform;

        [SerializeField]
        private Transform topPositionTransform;

        [SerializeField]
        private UITextPro _dialogText;

        [SerializeField]
        private RectTransform cursorPositionTransform;

        [SerializeField]
        private RectTransform arrowTransform;

        [SerializeField]
        private UIButton tapAnywhereButton;

        private RectTransform canvasRectTransform;

        protected override void Awake() {
            base.Awake();
            canvasRectTransform = GetComponent<RectTransform>();
            cursorPositionTransform.gameObject.SetActive(false);
        }

        public override void Refresh() {
            base.Refresh();
            onboardingHeroBehaviour.RefreshUI();
        }

        public event Action<PointerEventData> onTapAnywhere {
            add {
                tapAnywhereButton.onButtonClick += value;
            }
            remove {
                tapAnywhereButton.onButtonClick -= value;
            }
        }

        public void SetDialogText(string text, bool hasContinueText) {
            if(hasContinueText) {
                text += "\n\nTap To Continue";
            }

            _dialogText.text = text;
        }

        public bool targetAreaEnabled {
            set {
                cursorPositionTransform.gameObject.SetActive(value);
            }
        }

        public void SetCursorPositionAndSize(IUIOnboardingItemLocator itemLocator, ArrowPosition arrowPosition) {

            //reset to zero, cuz inverse transform was calculated based on zero as well
            cursorPositionTransform.anchoredPosition = Vector2.zero;
            cursorPositionTransform.anchoredPosition = cursorPositionTransform.InverseTransformPoint(itemLocator.itemPosition);

            switch(arrowPosition) {
                case ArrowPosition.Bottom:
                    arrowTransform.localRotation = Quaternion.Euler(0, 0, 0);
                    arrowTransform.anchorMin = new Vector2(0.5f, 0);
                    arrowTransform.anchorMax = new Vector2(0.5f, 0);
                    arrowTransform.pivot = new Vector2(0.5f, 1);
                    break;
                case ArrowPosition.Left:
                    arrowTransform.localRotation = Quaternion.Euler(0, 0, 270);
                    arrowTransform.anchorMin = new Vector2(0, 0.5f);
                    arrowTransform.anchorMax = new Vector2(0, 0.5f);
                    arrowTransform.pivot = new Vector2(0.5f, 1);
                    break;
                case ArrowPosition.Right:
                    arrowTransform.localRotation = Quaternion.Euler(0, 0, 90);
                    arrowTransform.anchorMin = new Vector2(1, 0.5f);
                    arrowTransform.anchorMax = new Vector2(1, 0.5f);
                    arrowTransform.pivot = new Vector2(0.5f, 1);
                    break;
                case ArrowPosition.Top:
                    arrowTransform.localRotation = Quaternion.Euler(0, 0, 180);
                    arrowTransform.anchorMin = new Vector2(0.5f, 1);
                    arrowTransform.anchorMax = new Vector2(0.5f, 1);
                    arrowTransform.pivot = new Vector2(0.5f, 1);
                    break;
            }

            Vector2 sizeScaleMultiplier = canvasRectTransform.rect.size / itemLocator.actualReferenceResolution;
            Vector2 scaledSize = itemLocator.size * sizeScaleMultiplier;

            cursorPositionTransform.sizeDelta = scaledSize;
        }

        public void SetDialogBox(DialogBoxPosition dialogBoxPosition) {
            switch(dialogBoxPosition) {
                case DialogBoxPosition.Top:
                    mainDialogTransform.SetParent(topPositionTransform, false);
                    break;
                case DialogBoxPosition.Bottom:
                    mainDialogTransform.SetParent(bottomPositionTransform, false);
                    break;
            }
        }

    }

}